﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;
using System.Text.RegularExpressions;

namespace GUI
{
    public partial class btnxemdiem : Form
    {

        public btnxemdiem()
        {
            InitializeComponent();
        }

        private void Form_XEMDIEM_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //DTO_SINHVIEN dtsv = new DTO_SINHVIEN(textBox1.Text);
            //DTO_HOCKI dthk = new DTO_HOCKI(cmbhocky.Text);
            //dtgxemdiem.DataSource = BLL_KETQUA.loaddldiem(dtsv, dthk);
        }

        //private void exportExcel(DataGridView dtg, string duongdan, string tentep)
        //{
        //    app obj = new app();
        //    obj.Application.Workbooks.Add(Type.Missing);
        //    obj.Columns.ColumnWidth = 25;
        //    for(int i =1; i< dtg.Columns.Count + 1; i++)
        //    {
        //        obj.Cells[1, i] = dtg.Columns[i - 1].HeaderText;
        //    }
        //    for (int i = 0; i < dtg.Rows.Count;i++ )
        //    {
        //        for (int j = 0; j < dtg.Rows.Count;j++ )
        //        {
        //            if (dtg.Rows[i].Cells[j].Value != null)
        //                obj.Cells[i + 2, j + 1] = dtg.Rows[i].Cells[j].Value.ToString();
        //        }
        //        obj.ActiveWorkbook.SaveCopyAs(duongdan + tentep + ".xlsx");
        //        obj.ActiveWorkbook.Saved = true;
        //    }
        //}
        private void button2_Click(object sender, EventArgs e)
        {
            
            saveFileDialog1.InitialDirectory = "C:";
            saveFileDialog1.Title = "Save as ";
            saveFileDialog1.FileName = "BangDiemSinhVien";
            saveFileDialog1.Filter = "Excel Files(2010)|*.xlsx";
            
            if(saveFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
               Microsoft.Office.Interop.Excel.Application excellapp = new Microsoft.Office.Interop.Excel.Application();
                excellapp.Application.Workbooks.Add(Type.Missing);
                excellapp.Columns.ColumnWidth = 20;
                for (int i = 1; i < dtgxemdiem.Columns.Count + 1; i++ )
                {
                    excellapp.Cells[1, i] = dtgxemdiem.Columns[i - 1].HeaderText;
                }
                for (int i = 0; i < dtgxemdiem.Rows.Count; i++ )
                {
                    for (int j = 0; j < dtgxemdiem.Columns.Count; j++ )
                    {
                        if (dtgxemdiem.Rows[i].Cells[j].Value != null)
                        excellapp.Cells[i + 2, j + 1] = dtgxemdiem.Rows[i].Cells[j].Value.ToString();
                    }
                    excellapp.ActiveWorkbook.SaveCopyAs(saveFileDialog1.FileName.ToString());
                    excellapp.ActiveWorkbook.Saved = true;

                }
                excellapp.Quit();
                MessageBox.Show("Xuất dữ liệu thành công");
            }
           
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (cmbhocky.Text == "")
                MessageBox.Show("Bạn chưa chọn Học Ki!");
            if (textBox1.Text == "")
                MessageBox.Show("Bạn chưa nhập Mã Sinh Viên");
            if(cmbhocky.Text!="" && textBox1.Text!="")
            {
            DTO_SINHVIEN dtsv = new DTO_SINHVIEN(textBox1.Text);
            DTO_HOCKI dthk = new DTO_HOCKI(cmbhocky.Text);
            dtgxemdiem.DataSource = BLL_KETQUA.loaddldiem(dtsv, dthk);
            }
            
            

        }

        private void btndiemtoankhoa_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
                MessageBox.Show("Bạn chưa nhập Mã Sinh Viên");
            else
            {
                DTO_SINHVIEN dtsv = new DTO_SINHVIEN(textBox1.Text);
                dtgxemdiem.DataSource = BLL_KETQUA.xemtoanfullhocki(dtsv);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
                if (cmbhocky.Text == "")
                MessageBox.Show("Bạn chưa chọn Học Ki!");
            if (textBox1.Text == "")
                MessageBox.Show("Bạn chưa nhập Mã Sinh Viên");
            if (cmbhocky.Text != "" && textBox1.Text != "")
            {
                DTO_SINHVIEN dtsv = new DTO_SINHVIEN(textBox1.Text);
                DTO_HOCKI dthk = new DTO_HOCKI(cmbhocky.Text);
                dtgxemdiem.DataSource = BLL_KETQUA.diemtong1ki(dtsv, dthk);
            }
        }
    }
}
