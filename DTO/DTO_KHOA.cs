﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_KHOA
    {
        string _makhoa;

        public string Makhoa
        {
            get { return _makhoa; }
            set { _makhoa = value; }
        }
        string _tenkhoa;

        public string Tenkhoa
        {
            get { return _tenkhoa; }
            set { _tenkhoa = value; }
        }
        public DTO_KHOA(string tmakhoa, string ttenkhoa)
        {
            _makhoa = tmakhoa;
            _tenkhoa = ttenkhoa;
        }
        public DTO_KHOA(string tmakhoa)
        {
            _makhoa = tmakhoa;
        }
    }
}
