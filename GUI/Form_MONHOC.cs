﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;
using DAL;
namespace GUI
{
    public partial class Form_MONHOC : Form
    {
        DataTable dt = new DataTable();
        public Form_MONHOC()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtgmonhoc_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dtgmonhoc.CurrentRow.Index;
            cmbmamon.Text = dtgmonhoc[0, index].Value.ToString();
            txttenmon.Text = dtgmonhoc[1, index].Value.ToString();
            txtsotinchi.Text = dtgmonhoc[2, index].Value.ToString();
            cmbhocki.Text = dtgmonhoc[3, index].Value.ToString();
            txtmakhoa.Text = dtgmonhoc[4, index].Value.ToString();

        }

        private void Form_MONHOC_Load(object sender, EventArgs e)
        {
            cmbhocki.DataSource = BLL_HOCKI.loaddlcmbhocki();
            cmbhocki.DisplayMember = "hocki";
            cmbhocki.ValueMember = "hocki";

            string temp = cmbhocki.SelectedValue.ToString();
            DTO_HOCKI dthk = new DTO_HOCKI(temp);
            dt = BLL_HOCKI.loaddlcmbmamon(dthk);
            cmbmamon.DataSource = dt;
            cmbmamon.DisplayMember = "MAMON";
            cmbmamon.ValueMember = "MAMON";

           

            dtgmonhoc.DataSource = BLL_MONHOC.loadmamonhoc();
            
        }

        private void btnthemmonhoc_Click(object sender, EventArgs e)
        {
            try
            {
                string txthocki = cmbhocki.SelectedValue.ToString();
              string txtmamon = cmbmamon.SelectedValue.ToString();
             DTO_MONHOC dtmh = new DTO_MONHOC(txtmamon, txttenmon.Text,int.Parse(txtsotinchi.Text),txthocki,txtmakhoa.Text);
            BLL_MONHOC.themmonhoc(dtmh);
            dtgmonhoc.DataSource = BLL_MONHOC.loadmamonhoc();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         finally
            {
                DAL_MONHOC.con.Close();
            }
            
        }

        private void btnxoamonhoc_Click(object sender, EventArgs e)
        {
            string txthocki = cmbhocki.SelectedValue.ToString();
            string txtmamon = cmbmamon.SelectedValue.ToString();
            DTO_MONHOC dtmh = new DTO_MONHOC(txtmamon, txttenmon.Text, int.Parse(txtsotinchi.Text), txthocki, txtmakhoa.Text);
            BLL_MONHOC.xoamonhoc(dtmh);
            dtgmonhoc.DataSource = BLL_MONHOC.loadmamonhoc();
        }

        private void btnsuamonhoc_Click(object sender, EventArgs e)
        {
            try
            {
                string txthocki = cmbhocki.SelectedValue.ToString();
                string txtmamon = cmbmamon.SelectedValue.ToString();
                DTO_MONHOC dtmh = new DTO_MONHOC(txtmamon, txttenmon.Text, int.Parse(txtsotinchi.Text), txthocki, txtmakhoa.Text);
                BLL_MONHOC.suamonhoc(dtmh);
                dtgmonhoc.DataSource = BLL_MONHOC.loadmamonhoc();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                DAL_MONHOC.con.Close();
            }
           
        }

        private void btnxoatrung_Click(object sender, EventArgs e)
        {
            BLL_MONHOC.xoadltrung();
            dtgmonhoc.DataSource = BLL_MONHOC.loadmamonhoc();
        }

        private void cmbhocki_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }

        private void cmbmamon_SelectedValueChanged(object sender, EventArgs e)
        {
           
        }

        private void cmbhocki_SizeChanged(object sender, EventArgs e)
        {

        }

        private void cmbhocki_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cmbmamon.ResetText();
            string temp = cmbhocki.SelectedValue.ToString();
            DTO_HOCKI dthk = new DTO_HOCKI(temp);
            dt = BLL_HOCKI.loaddlcmbmamon(dthk);
            cmbmamon.DataSource = dt;
            cmbmamon.DisplayMember = "MAMON";
            cmbmamon.ValueMember = "MAMON";
        }
    }
}
