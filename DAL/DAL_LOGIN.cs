﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using DTO;
using System.Security.Cryptography;

namespace DAL
{
  public class DAL_LOGIN
    {
        static string constring = ConfigurationManager.ConnectionStrings["qldiemsv"].ToString();
        static SqlConnection con = new SqlConnection(constring);
      public static DataTable loadthongtinformphanquyen()
        {
            SqlCommand cmd = new SqlCommand("select * from table_login", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
    
      public static DataTable laythongtindangnhap(DTO_LOGIN log)
      {
          SqlCommand cmd = new SqlCommand("dangnhap", con);
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@madn", log.Madn);
          cmd.Parameters.AddWithValue("@matkhau",log.Matkhau);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          con.Open();
          DataTable dt = new DataTable();
          da.Fill(dt);
          con.Close();
          return dt;

      }
      public static DataTable thongtinphanquyen(DTO_LOGIN log)
      {

          SqlCommand cmd = new SqlCommand("phanquyendangnhap", con);
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@madn", log.Madn);
          cmd.Parameters.AddWithValue("@matkhau", log.Matkhau);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          con.Open();
          DataTable dt = new DataTable();
          da.Fill(dt);
          con.Close();
          return dt;
      }
      public static DataTable doimatkhau(DTO_LOGIN log)
      {
          SqlCommand cmd = new SqlCommand("select madn from table_login where madn = @madn", con);
        //  cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@madn", log.Madn);
          //cmd.Parameters.AddWithValue("@matkhau", log.Matkhau);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          con.Open();
          DataTable dt = new DataTable();
          da.Fill(dt);
          con.Close();
          return dt;
      }
      public static void suamatkhau (DTO_LOGIN log)
      {
          SqlCommand cmd = new SqlCommand("suadoimatkhau",con);
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@matkhau",log.Matkhau);
          cmd.Parameters.AddWithValue("@madn", log.Madn);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          con.Open();
          cmd.ExecuteNonQuery();
          con.Close();
      }
      public static void themphanquyen (DTO_LOGIN log)
      {
         
              SqlCommand cmd = new SqlCommand("insert into table_login values (@madn,@matkhau,@quyen)", con);
              cmd.Parameters.AddWithValue("@madn", log.Madn);
              cmd.Parameters.AddWithValue("@matkhau", log.Matkhau);
              cmd.Parameters.AddWithValue("@quyen", log.Quyen);
              SqlDataAdapter da = new SqlDataAdapter(cmd);
              con.Open();
              cmd.ExecuteNonQuery();
              con.Close();    
      }
      public static void suaphanquyen(DTO_LOGIN log)
      {
          SqlCommand cmd = new SqlCommand("update table_login set quyen=@quyen where madn =@madn and matkhau = @matkhau", con);
          cmd.Parameters.AddWithValue("@madn", log.Madn);
          cmd.Parameters.AddWithValue("@matkhau", log.Matkhau);
          cmd.Parameters.AddWithValue("@quyen", log.Quyen);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          con.Open();
          cmd.ExecuteNonQuery();
          con.Close();
      }
      public static void xoaphanquyen(DTO_LOGIN log)
      {
          SqlCommand cmd = new SqlCommand("delete from table_login where madn =@madn and matkhau = @matkhau ", con);
          cmd.Parameters.AddWithValue("@madn", log.Madn);
          cmd.Parameters.AddWithValue("@matkhau", log.Matkhau);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          con.Open();
          cmd.ExecuteNonQuery();
          con.Close();
      }
      
    }
}
