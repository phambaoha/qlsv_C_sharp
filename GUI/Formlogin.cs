﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;
using System.Security.Cryptography;

namespace GUI
{
    public partial class Formlogin : Form
    {
        public static int dem = 0;
        public string temp1;
        public string temp2;
        public Formlogin(string Temp1, string TEMP2):this()
        {
            temp1 = Temp1;
            temp2 = TEMP2;
            txtmatkhau.Text = temp1;
            txttendn.Text = temp2;
        }
        public static string a;
        public static string  b;
        static DataTable dt = new DataTable();
        static DataTable dt1 = new DataTable();
        public Formlogin()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult traloi;
            traloi = MessageBox.Show("Bạn có muốn thoát không?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (traloi == DialogResult.Yes)
                Application.Exit();
        }


        // ma hoa mat khua bang md5

        public string GetMD5(string chuoi)
        {
            string str_md5 = "";
            byte[] mang = System.Text.Encoding.UTF8.GetBytes(chuoi);

            MD5CryptoServiceProvider my_md5 = new MD5CryptoServiceProvider();
            mang = my_md5.ComputeHash(mang);

            foreach (byte b in mang)
            {
                str_md5 += b.ToString("X2");
            }

            return str_md5;
        }
        private void btndangnhap_Click(object sender, EventArgs e)
        {
            txtimahepath frmmain = new txtimahepath();
            DTO_LOGIN dtlogin = new DTO_LOGIN(txttendn.Text ,GetMD5(txtmatkhau.Text));
            
            dt = BLL_LOGIN.laythongtindangnhap(dtlogin);
            dt1 = BLL_LOGIN.thongtinphanquyen(dtlogin);
            if (dt.Rows.Count == 1)
            {
                a = txtmatkhau.Text;
                b = txttendn.Text; 
                this.Hide();
                txtimahepath.phanquyen = dt1.Rows[0][0].ToString().Trim();
                frmmain.ShowDialog();
            }
            else
            {
                MessageBox.Show("Tên Đăng Nhập hoặc Mật Khẩu không đúng, Mời Nhập Lại", "Thông Báo");
                txtmatkhau.ResetText();
                txttendn.ResetText();
                txttendn.Focus();
            }
        }

        private void Formlogin_Load(object sender, EventArgs e)
        {
            txttendn.Focus();   
        }

       private void chb_nhotaikhoan_CheckedChanged(object sender, EventArgs e)
        {
            
            if (chb_nhotaikhoan.Checked==true)
            {
                dem = 1;
            }
        }

        private void chb_hienmatkhau_CheckedChanged(object sender, EventArgs e)
        {
            if (chb_hienmatkhau.Checked == true)
            {

                txtmatkhau.UseSystemPasswordChar = false;
            }
            if (chb_hienmatkhau.Checked == false)
                txtmatkhau.UseSystemPasswordChar = true;
        }

        private void txtmatkhau_TextChanged(object sender, EventArgs e)
        {
            txtmatkhau.UseSystemPasswordChar = true;
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }
    }
}
