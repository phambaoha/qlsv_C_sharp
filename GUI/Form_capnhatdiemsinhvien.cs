﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class cmbtenmon : Form
    {
        
        static DataTable dt = new DataTable();
        static DataTable dt1 = new DataTable();
        public cmbtenmon()
        {
            InitializeComponent();
        }

        private void Form_capnhatdiemsinhvien_Load(object sender, EventArgs e)
        {
            cmbhocki.DataSource = BLL_HOCKI.loaddlcmbhocki();
            cmbhocki.DisplayMember = "hocki";
            cmbhocki.ValueMember = "hocki";

            cmbmalop.DataSource = BLL_LOP.loaddlcomboxmalop();
            cmbmalop.DisplayMember = "malop";
            cmbmalop.ValueMember = "malop";

            comboBox1.DataSource = BLL_KHOA.laydlmakhoa();
            comboBox1.DisplayMember = "makhoa";
            comboBox1.ValueMember = "makhoa";

            cmbmamon.ResetText();

            string temp = cmbhocki.SelectedValue.ToString();
            DTO_HOCKI dthk = new DTO_HOCKI(temp);
            dt = BLL_HOCKI.loaddlcmbmamon(dthk);
            cmbmamon.DataSource = dt;
            cmbmamon.DisplayMember = "MAMON";
            cmbmamon.ValueMember = "MAMON";

            dt = BLL_KETQUA.loaddlcomboboxhocki();
            dtgcapnhatdiemsv.DataSource = dt;

            updatetable_ketqua(sender, e);

        }
       private void updatetable_ketqua(object sender, EventArgs e)
        {
            BLL_KETQUA.updatetable_ketqua();
            dtgcapnhatdiemsv.DataSource = BLL_KETQUA.loaddlcomboboxhocki();
        }
        private void cmbhocki_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        private void cmbhocki_SelectedValueChanged(object sender, EventArgs e)
        {
            cmbmamon.ResetText();
            string temp = cmbhocki.SelectedValue.ToString();
            DTO_HOCKI dthk = new DTO_HOCKI(temp);
            dt = BLL_HOCKI.loaddlcmbmamon(dthk);
            cmbmamon.DataSource = dt;
            cmbmamon.DisplayMember = "MAMON";
            cmbmamon.ValueMember = "MAMON";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbtenmonhoc_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                string temp = cmbmamon.SelectedValue.ToString();
                DTO_MONHOC dtmh = new DTO_MONHOC(temp);
                dt = BLL_MONHOC.loadsotinchi(dtmh);
                txtsotc.Text = dt.Rows[0][0].ToString();
            }
            catch (System.Exception ex)
            {

            }


        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void cbhoten_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int temp = 0;

            try
            {
                int index = 3; int indx = 0;
                for (int i = 0; i < dtgcapnhatdiemsv.Rows.Count; i++)
                {
                    if ((cmbmamon.SelectedValue.ToString() == dtgcapnhatdiemsv.Rows[i].Cells[index].Value.ToString()) && (cbmasv.SelectedValue.ToString() == dtgcapnhatdiemsv.Rows[i].Cells[indx].Value.ToString()))
                    {
                        temp = 1;
                        MessageBox.Show("Sinh viên đã học môn này rồi!");
                    }
                }
            }
            catch (System.Exception ex)
            {

            }

            if (temp == 0)
            {
                try
                {

                    float diem_a = float.Parse(txtdiema.Text);
                    float diem_b = float.Parse(txtdiemb.Text);
                    float diem_c = float.Parse(txtdiemc.Text);
                    float diem_tong;
                    float.TryParse(txtdiemtongmonhoc.Text, out diem_tong);
                    int diem_rl = int.Parse(txtdiemrenluyen.Text);
                    DTO_KETQUA dtkq = new DTO_KETQUA(cbmasv.Text.ToString().Trim(), txthoten.Text.ToString().Trim(), cmbhocki.Text.ToString().Trim(), cmbmamon.Text.ToString().Trim(), cmbmalop.Text.ToString().Trim(), diem_a, diem_b, diem_c, diem_tong, txtdiemchu1.Text, float.Parse(txtheso.Text), diem_rl, int.Parse(txtsotc.Text), float.Parse(txtdiemtonghebon.Text));
                    BLL_KETQUA.themdiemsv(dtkq);
                    dtgcapnhatdiemsv.DataSource = BLL_KETQUA.loaddlcomboboxhocki();
                    MessageBox.Show("Thêm Thành Công");

                }
                catch (System.Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtdiemtong_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

            try
            {

                float A = float.Parse(txtdiema.Text);
                float B = float.Parse(txtdiemb.Text);
                float C = float.Parse(txtdiemc.Text);
                int E = int.Parse(txtdiemrenluyen.Text);
                if (A < 0 || A > 10 || B < 0 || B > 10 || C < 0 || C > 10 || E < 0 || E > 100)
                {
                    MessageBox.Show("Điểm sai phạm vi cho phép, Nhập lại!");
                }
                else
                {
                    float tong = (float)(A * 6.0 / 10 + B * 3.0 / 10 + C * 1.0 / 10);
                    txtdiemtongmonhoc.Text = tong.ToString();
                    float D = (float)(int.Parse(txtsotc.Text) * float.Parse(txtheso.Text));
                    txtdiemtonghebon.Text = D.ToString();
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Fail! " + ex.Message);
            }

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void txtdiema_TextChanged(object sender, EventArgs e)
        {


        }

        private void txtdiemrenluyen_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtgcapnhatdiemsv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dtgcapnhatdiemsv.CurrentRow.Index;
            cbmasv.Text = dtgcapnhatdiemsv[0, index].Value.ToString();
            txthoten.Text = dtgcapnhatdiemsv[1, index].Value.ToString();
            cmbhocki.Text = dtgcapnhatdiemsv[2, index].Value.ToString();
            cmbmamon.Text = dtgcapnhatdiemsv[3, index].Value.ToString();
            cmbmalop.Text = dtgcapnhatdiemsv[4, index].Value.ToString();
            txtdiema.Text = dtgcapnhatdiemsv[5, index].Value.ToString();
            txtdiemb.Text = dtgcapnhatdiemsv[6, index].Value.ToString();
            txtdiemc.Text = dtgcapnhatdiemsv[7, index].Value.ToString();
            txtdiemtongmonhoc.Text = dtgcapnhatdiemsv[8, index].Value.ToString();
            txtdiemchu1.Text = dtgcapnhatdiemsv[9, index].Value.ToString();
            txtheso.Text = dtgcapnhatdiemsv[10, index].Value.ToString();
            txtdiemrenluyen.Text = dtgcapnhatdiemsv[11, index].Value.ToString();
            txtsotc.Text = dtgcapnhatdiemsv[12, index].Value.ToString();
        }
        private void txthoten_TextChanged(object sender, EventArgs e)
        {

        }
        private void cbmasv_SelectedValueChanged(object sender, EventArgs e)
        {
            txthoten.ResetText();
            txthoten.Focus();
            try
            {

                string temp = cbmasv.SelectedValue.ToString();
                DTO_SINHVIEN dtsv = new DTO_SINHVIEN(temp);
                dt = BLL_SINHVIEN.loadddtxthoten(dtsv);
                txthoten.Text = dt.Rows[0][0].ToString().Trim();
            }
            catch (System.Exception ex)
            {

            }

        }
        private void cmbmalop_SelectedValueChanged(object sender, EventArgs e)
        {
            cbmasv.ResetText();
            txthoten.ResetText();
            string temp = cmbmalop.SelectedValue.ToString();
            DTO_LOP dtlp = new DTO_LOP(temp);
            dt = BLL_SINHVIEN.loaddulieucomboboxhoten(dtlp);
            cbmasv.DataSource = dt;
            cbmasv.DisplayMember = "masv";
            cbmasv.ValueMember = "masv";
        }

        private void cmbmalop_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtdiemtongmonhoc_TextChanged(object sender, EventArgs e)
        {
            try
            {
                float diemtong = float.Parse(txtdiemtongmonhoc.Text);
                if (diemtong < 4)
                {
                    txtdiemchu1.Text = "F".Trim();
                }
                else
                    if (diemtong >= 4 && diemtong < 5)
                    { txtdiemchu1.Text = "D".Trim(); }
                    else
                        if (diemtong >= 5 && diemtong < 5.5)
                        { txtdiemchu1.Text = "D+".Trim(); }
                        else
                            if (diemtong >= 5.5 && diemtong < 6.5)
                            { txtdiemchu1.Text = "C".Trim(); }
                            else
                                if (diemtong >= 6.5 && diemtong < 7)
                                { txtdiemchu1.Text = "C+".Trim(); }

                                else
                                    if (diemtong >= 7 && diemtong < 8)
                                    { txtdiemchu1.Text = "B".Trim(); }
                                    else
                                        if (diemtong >= 8 && diemtong < 8.5)
                                        { txtdiemchu1.Text = "B+".Trim(); }
                                        else
                                            if (diemtong >= 8.5 && diemtong <= 10)
                                            { txtdiemchu1.Text = "A".Trim(); }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        public void button2_Click(object sender, EventArgs e)
        {
            DTO_KETQUA dtkq = new DTO_KETQUA(cbmasv.Text, cmbmamon.Text);
            BLL_KETQUA.Xoadiemsv(dtkq);
            dtgcapnhatdiemsv.DataSource = BLL_KETQUA.loaddlcomboboxhocki();
            MessageBox.Show("Xoá Thành Công");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {    float diem_a = float.Parse(txtdiema.Text);
                float diem_b = float.Parse(txtdiemb.Text);
                float diem_c = float.Parse(txtdiemc.Text);
                float diem_tong = float.Parse(txtdiemtongmonhoc.Text);
                int diem_rl = int.Parse(txtdiemrenluyen.Text);
                DTO_KETQUA dtkq = new DTO_KETQUA(cbmasv.Text.ToString().Trim(), txthoten.Text.ToString().Trim(), cmbhocki.Text.ToString().Trim(), cmbmamon.Text.ToString().Trim(), cmbmalop.Text.ToString().Trim(), diem_a, diem_b, diem_c, diem_tong, txtdiemchu1.Text, float.Parse(txtheso.Text), diem_rl, int.Parse(txtsotc.Text), float.Parse(txtdiemtonghebon.Text));
                BLL_KETQUA.suadiemsv(dtkq);
                dtgcapnhatdiemsv.DataSource = BLL_KETQUA.loaddlcomboboxhocki();
                MessageBox.Show("Sửa Thành Công");
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }    

        }

        private void txtdiemchu1_TextChanged(object sender, EventArgs e)
        {
            if (txtdiemchu1.Text == "F")
                txtheso.Text = "0";
            if (txtdiemchu1.Text == "D")
                txtheso.Text = "1";
            if (txtdiemchu1.Text == "D+")
                txtheso.Text = "1.5";
            if (txtdiemchu1.Text == "C")
                txtheso.Text = "2";
            if (txtdiemchu1.Text == "C+")
                txtheso.Text = "2.5";
            if (txtdiemchu1.Text == "B")
                txtheso.Text = "3";
            if (txtdiemchu1.Text == "B+")
                txtheso.Text = "3.5";
            if (txtdiemchu1.Text == "A")
                txtheso.Text = "4";
        }

        private void comboBox1_SelectedIndexChanged_2(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            string temp1 = comboBox1.SelectedValue.ToString();
            DTO_KHOA dtkhoa = new DTO_KHOA(temp1);
            dt1 = BLL_KHOA.loadcomboboxmalop(dtkhoa);
            cmbmalop.DataSource = dt1;
            cmbmalop.DisplayMember = "malop";
            cmbmalop.ValueMember = "malop";
        }

        private void txtsotc_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            string ml = cmbmalop.SelectedValue.ToString();
            DTO_LOP dtlp = new DTO_LOP(ml);
            dtgcapnhatdiemsv.DataSource = BLL_KETQUA.timkiemmalop(dtlp);
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            string ml = cmbhocki.SelectedValue.ToString();
            DTO_HOCKI dthk = new DTO_HOCKI(ml);
            dtgcapnhatdiemsv.DataSource = BLL_KETQUA.timkiemhocki(dthk);
        }

        private void btnreset_Click(object sender, EventArgs e)
        {

            dtgcapnhatdiemsv.DataSource = BLL_KETQUA.loaddlcomboboxhocki();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string ml = cmbmamon.SelectedValue.ToString();
            DTO_MONHOC dtmh = new DTO_MONHOC(ml);
            dtgcapnhatdiemsv.DataSource = BLL_KETQUA.timkiemmamon(dtmh);
        }

        private void button9_Click(object sender, EventArgs e)
        {          
            DTO_SINHVIEN dtsv = new DTO_SINHVIEN(cbmasv.Text);
            dtgcapnhatdiemsv.DataSource = BLL_KETQUA.timkiemmasv(dtsv);
        }
    }
}
