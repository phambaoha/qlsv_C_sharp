﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
namespace GUI
{
    public partial class form_thongke : Form
    {
        public form_thongke()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void form_thongke_Load(object sender, EventArgs e)
        {
            cmbhocki.DataSource = BLL_HOCKI.loaddlcmbhocki();
            cmbhocki.DisplayMember = "hocki";
            cmbhocki.ValueMember = "hocki";

            radio_tatca.Checked = true;
            cmb_malop.DataSource = BLL_LOP.loaddlcomboxmalop();
            cmb_malop.DisplayMember = "malop";
            cmb_malop.ValueMember = "malop";
        }

        private void btn_loc_Click(object sender, EventArgs e)
        {
            
            if (radio_tatca.Checked)
            {
                string malop1 = cmb_malop.SelectedValue.ToString();
                DTO_LOP dtsv = new DTO_LOP(malop1);
                dtg_thongke.DataSource = BLL_LOP.xemtoanfullhockitheomalop_thongke(dtsv);
            }
            if(radio_gioi.Checked)
            {
                string malop2 = cmb_malop.SelectedValue.ToString();
                DTO_LOP dtlp = new DTO_LOP(malop2);
                dtg_thongke.DataSource = BLL_LOP.loc_diem_gioi(dtlp);
            }
            if (radio_kha.Checked)
            {
                string malop3 = cmb_malop.SelectedValue.ToString();
                DTO_LOP dtlp1 = new DTO_LOP(malop3);
                dtg_thongke.DataSource = BLL_LOP.loc_diem_kha(dtlp1);
            }
            if (radio_tb.Checked)
            {
                string malop4 = cmb_malop.SelectedValue.ToString();
                DTO_LOP dtlp2 = new DTO_LOP(malop4);
                dtg_thongke.DataSource = BLL_LOP.loc_diem_tb(dtlp2);
            }
            if (radio_yeu.Checked)
            {
                string malop5 = cmb_malop.SelectedValue.ToString();
                DTO_LOP dtlp3 = new DTO_LOP(malop5);
                dtg_thongke.DataSource = BLL_LOP.loc_diem_yeu(dtlp3);
            }
            
        }

        private void dtg_thongke_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void btnloc2_Click(object sender, EventArgs e)
        {
            if (radiohocbong.Checked)
            {
                string hocki = cmbhocki.SelectedValue.ToString();
                DTO_HOCKI hk = new DTO_HOCKI(hocki);
                dtg_thongke.DataSource = BLL_HOCKI.hocbongsinhvien(hk);
            }
            if (radiocanhcao.Checked)
            {
                string hocki1 = cmbhocki.SelectedValue.ToString();
                DTO_HOCKI hk1 = new DTO_HOCKI(hocki1);
                dtg_thongke.DataSource = BLL_HOCKI.canhcaosinhvien(hk1);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {

            saveFileDialog2.InitialDirectory = "C:";
            saveFileDialog2.Title = "Save as ";
            saveFileDialog2.FileName = "DanhSachSV";
            saveFileDialog2.Filter = "Excel Files(2010)|*.xlsx";

            if (saveFileDialog2.ShowDialog() != DialogResult.Cancel)
            {
                Microsoft.Office.Interop.Excel.Application excellapp = new Microsoft.Office.Interop.Excel.Application();
                excellapp.Application.Workbooks.Add(Type.Missing);
                excellapp.Columns.ColumnWidth = 20;
                for (int i = 1; i < dtg_thongke.Columns.Count + 1; i++)
                {
                    excellapp.Cells[1, i] = dtg_thongke.Columns[i - 1].HeaderText;
                }
                for (int i = 0; i < dtg_thongke.Rows.Count; i++)
                {
                    for (int j = 0; j < dtg_thongke.Columns.Count; j++)
                    {
                        if (dtg_thongke.Rows[i].Cells[j].Value != null)
                            excellapp.Cells[i + 2, j + 1] = dtg_thongke.Rows[i].Cells[j].Value.ToString();
                    }
                    excellapp.ActiveWorkbook.SaveCopyAs(saveFileDialog2.FileName.ToString());
                    excellapp.ActiveWorkbook.Saved = true;

                }
                excellapp.Quit();
                MessageBox.Show("Xuất dữ liệu thành công");
            }
        }
    }
}
