﻿namespace GUI
{
    partial class Formrepprtthongtinsv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formrepprtthongtinsv));
            this.Table_SINHVIENBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataSetthongtinsv = new GUI.DataSetthongtinsv();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.cmbreportthongtinsv = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Table_SINHVIENTableAdapter = new GUI.DataSetthongtinsvTableAdapters.Table_SINHVIENTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.Table_SINHVIENBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetthongtinsv)).BeginInit();
            this.SuspendLayout();
            // 
            // Table_SINHVIENBindingSource
            // 
            this.Table_SINHVIENBindingSource.DataMember = "Table_SINHVIEN";
            this.Table_SINHVIENBindingSource.DataSource = this.DataSetthongtinsv;
            // 
            // DataSetthongtinsv
            // 
            this.DataSetthongtinsv.DataSetName = "DataSetthongtinsv";
            this.DataSetthongtinsv.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.Table_SINHVIENBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "GUI.Report1.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(2, 65);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(649, 312);
            this.reportViewer1.TabIndex = 0;
            // 
            // cmbreportthongtinsv
            // 
            this.cmbreportthongtinsv.FormattingEnabled = true;
            this.cmbreportthongtinsv.Location = new System.Drawing.Point(74, 22);
            this.cmbreportthongtinsv.Name = "cmbreportthongtinsv";
            this.cmbreportthongtinsv.Size = new System.Drawing.Size(146, 21);
            this.cmbreportthongtinsv.TabIndex = 1;
            this.cmbreportthongtinsv.SelectedIndexChanged += new System.EventHandler(this.cmbreportthongtinsv_SelectedIndexChanged);
            this.cmbreportthongtinsv.SelectedValueChanged += new System.EventHandler(this.cmbreportthongtinsv_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Mã Lớp";
            // 
            // Table_SINHVIENTableAdapter
            // 
            this.Table_SINHVIENTableAdapter.ClearBeforeFill = true;
            // 
            // Formrepprtthongtinsv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 370);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbreportthongtinsv);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formrepprtthongtinsv";
            this.Text = "Thông Tin Sinh Viên";
            this.Load += new System.EventHandler(this.Formrepprtthongtinsv_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Table_SINHVIENBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetthongtinsv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.ComboBox cmbreportthongtinsv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource Table_SINHVIENBindingSource;
        private DataSetthongtinsv DataSetthongtinsv;
        private DataSetthongtinsvTableAdapters.Table_SINHVIENTableAdapter Table_SINHVIENTableAdapter;
    }
}