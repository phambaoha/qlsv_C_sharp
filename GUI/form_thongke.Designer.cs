﻿namespace GUI
{
    partial class form_thongke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_thongke));
            this.btn_loc = new System.Windows.Forms.Button();
            this.dtg_thongke = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radio_tatca = new System.Windows.Forms.RadioButton();
            this.cmb_malop = new System.Windows.Forms.ComboBox();
            this.radio_yeu = new System.Windows.Forms.RadioButton();
            this.radio_kha = new System.Windows.Forms.RadioButton();
            this.radio_tb = new System.Windows.Forms.RadioButton();
            this.radio_gioi = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.radiohocbong = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbhocki = new System.Windows.Forms.ComboBox();
            this.btnloc2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radiocanhcao = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_thongke)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_loc
            // 
            this.btn_loc.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_loc.Location = new System.Drawing.Point(317, 68);
            this.btn_loc.Name = "btn_loc";
            this.btn_loc.Size = new System.Drawing.Size(75, 56);
            this.btn_loc.TabIndex = 7;
            this.btn_loc.Text = "LỌC";
            this.btn_loc.UseVisualStyleBackColor = false;
            this.btn_loc.Click += new System.EventHandler(this.btn_loc_Click);
            // 
            // dtg_thongke
            // 
            this.dtg_thongke.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_thongke.Location = new System.Drawing.Point(444, 12);
            this.dtg_thongke.Name = "dtg_thongke";
            this.dtg_thongke.Size = new System.Drawing.Size(455, 257);
            this.dtg_thongke.TabIndex = 8;
            this.dtg_thongke.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_thongke_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radio_tatca);
            this.groupBox1.Controls.Add(this.cmb_malop);
            this.groupBox1.Controls.Add(this.btn_loc);
            this.groupBox1.Controls.Add(this.radio_yeu);
            this.groupBox1.Controls.Add(this.radio_kha);
            this.groupBox1.Controls.Add(this.radio_tb);
            this.groupBox1.Controls.Add(this.radio_gioi);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(417, 154);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // radio_tatca
            // 
            this.radio_tatca.AutoSize = true;
            this.radio_tatca.Location = new System.Drawing.Point(99, 61);
            this.radio_tatca.Name = "radio_tatca";
            this.radio_tatca.Size = new System.Drawing.Size(44, 17);
            this.radio_tatca.TabIndex = 17;
            this.radio_tatca.TabStop = true;
            this.radio_tatca.Text = "ALL";
            this.radio_tatca.UseVisualStyleBackColor = true;
            // 
            // cmb_malop
            // 
            this.cmb_malop.FormattingEnabled = true;
            this.cmb_malop.Location = new System.Drawing.Point(99, 19);
            this.cmb_malop.Name = "cmb_malop";
            this.cmb_malop.Size = new System.Drawing.Size(174, 21);
            this.cmb_malop.TabIndex = 16;
            // 
            // radio_yeu
            // 
            this.radio_yeu.AutoSize = true;
            this.radio_yeu.Location = new System.Drawing.Point(193, 107);
            this.radio_yeu.Name = "radio_yeu";
            this.radio_yeu.Size = new System.Drawing.Size(80, 17);
            this.radio_yeu.TabIndex = 15;
            this.radio_yeu.TabStop = true;
            this.radio_yeu.Text = "Yếu(2->2.5)";
            this.radio_yeu.UseVisualStyleBackColor = true;
            // 
            // radio_kha
            // 
            this.radio_kha.AutoSize = true;
            this.radio_kha.Location = new System.Drawing.Point(193, 84);
            this.radio_kha.Name = "radio_kha";
            this.radio_kha.Size = new System.Drawing.Size(92, 17);
            this.radio_kha.TabIndex = 14;
            this.radio_kha.TabStop = true;
            this.radio_kha.Text = "Khá (2.5->3.2)";
            this.radio_kha.UseVisualStyleBackColor = true;
            // 
            // radio_tb
            // 
            this.radio_tb.AutoSize = true;
            this.radio_tb.Location = new System.Drawing.Point(99, 107);
            this.radio_tb.Name = "radio_tb";
            this.radio_tb.Size = new System.Drawing.Size(75, 17);
            this.radio_tb.TabIndex = 13;
            this.radio_tb.TabStop = true;
            this.radio_tb.Text = "TB(2->2.5)";
            this.radio_tb.UseVisualStyleBackColor = true;
            // 
            // radio_gioi
            // 
            this.radio_gioi.AutoSize = true;
            this.radio_gioi.Location = new System.Drawing.Point(99, 84);
            this.radio_gioi.Name = "radio_gioi";
            this.radio_gioi.Size = new System.Drawing.Size(79, 17);
            this.radio_gioi.TabIndex = 12;
            this.radio_gioi.TabStop = true;
            this.radio_gioi.Text = "Giỏi (>=3.2)";
            this.radio_gioi.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.DarkRed;
            this.label2.Location = new System.Drawing.Point(11, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Điểm tích luỹ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.DarkRed;
            this.label1.Location = new System.Drawing.Point(11, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Lớp";
            // 
            // radiohocbong
            // 
            this.radiohocbong.AutoSize = true;
            this.radiohocbong.Location = new System.Drawing.Point(25, 58);
            this.radiohocbong.Name = "radiohocbong";
            this.radiohocbong.Size = new System.Drawing.Size(140, 17);
            this.radiohocbong.TabIndex = 18;
            this.radiohocbong.TabStop = true;
            this.radiohocbong.Text = "Sinh Viên Đạt Học bổng";
            this.radiohocbong.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.DarkRed;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Học Kì";
            // 
            // cmbhocki
            // 
            this.cmbhocki.FormattingEnabled = true;
            this.cmbhocki.Location = new System.Drawing.Point(51, 19);
            this.cmbhocki.Name = "cmbhocki";
            this.cmbhocki.Size = new System.Drawing.Size(174, 21);
            this.cmbhocki.TabIndex = 18;
            // 
            // btnloc2
            // 
            this.btnloc2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnloc2.Location = new System.Drawing.Point(317, 58);
            this.btnloc2.Name = "btnloc2";
            this.btnloc2.Size = new System.Drawing.Size(75, 56);
            this.btnloc2.TabIndex = 18;
            this.btnloc2.Text = "LỌC";
            this.btnloc2.UseVisualStyleBackColor = false;
            this.btnloc2.Click += new System.EventHandler(this.btnloc2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radiocanhcao);
            this.groupBox2.Controls.Add(this.btnloc2);
            this.groupBox2.Controls.Add(this.radiohocbong);
            this.groupBox2.Controls.Add(this.cmbhocki);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 184);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(417, 160);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            // 
            // radiocanhcao
            // 
            this.radiocanhcao.AutoSize = true;
            this.radiocanhcao.Location = new System.Drawing.Point(25, 97);
            this.radiocanhcao.Name = "radiocanhcao";
            this.radiocanhcao.Size = new System.Drawing.Size(118, 17);
            this.radiocanhcao.TabIndex = 19;
            this.radiocanhcao.TabStop = true;
            this.radiocanhcao.Text = "Cảnh cáo Sinh viên";
            this.radiocanhcao.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(574, 301);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 45);
            this.button1.TabIndex = 20;
            this.button1.Text = "Thoát";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(728, 307);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(93, 39);
            this.button3.TabIndex = 22;
            this.button3.Text = "Xuất ra Excel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // form_thongke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.ClientSize = new System.Drawing.Size(911, 368);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dtg_thongke);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "form_thongke";
            this.Text = "Thống Kê";
            this.Load += new System.EventHandler(this.form_thongke_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtg_thongke)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_loc;
        private System.Windows.Forms.DataGridView dtg_thongke;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmb_malop;
        private System.Windows.Forms.RadioButton radio_yeu;
        private System.Windows.Forms.RadioButton radio_kha;
        private System.Windows.Forms.RadioButton radio_tb;
        private System.Windows.Forms.RadioButton radio_gioi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radio_tatca;
        private System.Windows.Forms.RadioButton radiohocbong;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbhocki;
        private System.Windows.Forms.Button btnloc2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radiocanhcao;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
    }
}