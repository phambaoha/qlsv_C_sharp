﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;

namespace GUI
{
    public partial class FORM_KHOA : Form
    {
        DataTable dt = new DataTable();
        public FORM_KHOA()
        {
            InitializeComponent();
        }

        private void FORM_KHOA_Load(object sender, EventArgs e)
        {
            dt = BLL_KHOA.laydlmakhoa();
            dtgkhoa.DataSource = dt;
             
        }

        private void btnthemkhoa_Click(object sender, EventArgs e)
        {

              DTO_KHOA kh = new DTO_KHOA(txtmakhoa.Text, txttenkhoa.Text);
              BLL_KHOA.themkhoa(kh);
              dtgkhoa.DataSource = BLL_KHOA.laydlmakhoa();
        }

        private void btnsuakhoa_Click(object sender, EventArgs e)
        {
            DTO_KHOA kh = new DTO_KHOA(txtmakhoa.Text,txttenkhoa.Text);
            BLL_KHOA.suakhoa(kh);
            dtgkhoa.DataSource = BLL_KHOA.laydlmakhoa();
        }

        private void btnxoakhoa_Click(object sender, EventArgs e)
        {
            DTO_KHOA kh = new DTO_KHOA(txtmakhoa.Text);
            BLL_KHOA.xoakhoa(kh);
            dtgkhoa.DataSource = BLL_KHOA.laydlmakhoa();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtgkhoa_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dtgkhoa.CurrentRow.Index;
            txtmakhoa.Text = dtgkhoa[0, index].Value.ToString();
            txttenkhoa.Text = dtgkhoa[1, index].Value.ToString();
              
        }
    }
}
