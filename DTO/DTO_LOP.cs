﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
  public  class DTO_LOP
    {
        string _malop;

        public string Malop
        {
            get { return _malop; }
            set { _malop = value; }
        }
        string _tenlop;

        public string Tenlop
        {
            get { return _tenlop; }
            set { _tenlop = value; }
        }
        string _makhoa;

        public string Makhoa
        {
            get { return _makhoa; }
            set { _makhoa = value; }
        }
        
        public DTO_LOP(string TMALOP, string TTENLOP,  String TMAKHOA)
        {
            _malop = TMALOP;
            _tenlop = TTENLOP;
            _makhoa = TMAKHOA;
        }
      public DTO_LOP(string tmalop)
        {
            _malop = tmalop;
        }
     

    }
}
