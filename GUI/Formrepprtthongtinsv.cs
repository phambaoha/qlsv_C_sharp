﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class Formrepprtthongtinsv : Form
    {
        public Formrepprtthongtinsv()
        {
            InitializeComponent();
        }

        private void Formrepprtthongtinsv_Load(object sender, EventArgs e)
        {
            cmbreportthongtinsv.DataSource = BLL_LOP.loaddlcomboxmalop();
            cmbreportthongtinsv.DisplayMember = "malop";
            cmbreportthongtinsv.ValueMember = "malop";
            // TODO: This line of code loads data into the 'DataSetthongtinsv.Table_SINHVIEN' table. You can move, or remove it, as needed.
          //  this.Table_SINHVIENTableAdapter.Fill(this.DataSetthongtinsv.Table_SINHVIEN);

            //this.reportViewer1.RefreshReport();
        }

        private void cmbreportthongtinsv_SelectedValueChanged(object sender, EventArgs e)
        {
            string temp = cmbreportthongtinsv.SelectedValue.ToString().Trim();
            this.Table_SINHVIENTableAdapter.Fill(this.DataSetthongtinsv.Table_SINHVIEN,temp);
            this.reportViewer1.RefreshReport();
        }

        private void cmbreportthongtinsv_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
