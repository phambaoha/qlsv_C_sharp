﻿namespace GUI
{
    partial class Formhocki
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txthocki = new System.Windows.Forms.TextBox();
            this.txtmamon = new System.Windows.Forms.TextBox();
            this.dtghocki = new System.Windows.Forms.DataGridView();
            this.hocki = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mamon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnthemhocki = new System.Windows.Forms.Button();
            this.btnsuahocki = new System.Windows.Forms.Button();
            this.btnxoahocki = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtghocki)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(294, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Học kì";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(294, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mã Môn Học";
            // 
            // txthocki
            // 
            this.txthocki.Location = new System.Drawing.Point(394, 44);
            this.txthocki.Name = "txthocki";
            this.txthocki.Size = new System.Drawing.Size(116, 20);
            this.txthocki.TabIndex = 2;
            // 
            // txtmamon
            // 
            this.txtmamon.Location = new System.Drawing.Point(394, 87);
            this.txtmamon.Name = "txtmamon";
            this.txtmamon.Size = new System.Drawing.Size(116, 20);
            this.txtmamon.TabIndex = 3;
            // 
            // dtghocki
            // 
            this.dtghocki.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtghocki.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtghocki.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.hocki,
            this.mamon});
            this.dtghocki.Location = new System.Drawing.Point(12, 29);
            this.dtghocki.Name = "dtghocki";
            this.dtghocki.Size = new System.Drawing.Size(276, 150);
            this.dtghocki.TabIndex = 4;
            this.dtghocki.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtghocki_CellContentClick);
            // 
            // hocki
            // 
            this.hocki.DataPropertyName = "hocki";
            this.hocki.HeaderText = "Học Kì";
            this.hocki.Name = "hocki";
            // 
            // mamon
            // 
            this.mamon.DataPropertyName = "mamon";
            this.mamon.HeaderText = "Mã Môn";
            this.mamon.Name = "mamon";
            // 
            // btnthemhocki
            // 
            this.btnthemhocki.ForeColor = System.Drawing.Color.Blue;
            this.btnthemhocki.Location = new System.Drawing.Point(34, 213);
            this.btnthemhocki.Name = "btnthemhocki";
            this.btnthemhocki.Size = new System.Drawing.Size(75, 23);
            this.btnthemhocki.TabIndex = 5;
            this.btnthemhocki.Text = "Thêm";
            this.btnthemhocki.UseVisualStyleBackColor = true;
            this.btnthemhocki.Click += new System.EventHandler(this.btnthemlop_Click);
            // 
            // btnsuahocki
            // 
            this.btnsuahocki.ForeColor = System.Drawing.Color.Blue;
            this.btnsuahocki.Location = new System.Drawing.Point(157, 213);
            this.btnsuahocki.Name = "btnsuahocki";
            this.btnsuahocki.Size = new System.Drawing.Size(75, 23);
            this.btnsuahocki.TabIndex = 6;
            this.btnsuahocki.Text = "Sửa";
            this.btnsuahocki.UseVisualStyleBackColor = true;
            this.btnsuahocki.Click += new System.EventHandler(this.btnsualop_Click);
            // 
            // btnxoahocki
            // 
            this.btnxoahocki.ForeColor = System.Drawing.Color.Blue;
            this.btnxoahocki.Location = new System.Drawing.Point(269, 213);
            this.btnxoahocki.Name = "btnxoahocki";
            this.btnxoahocki.Size = new System.Drawing.Size(75, 23);
            this.btnxoahocki.TabIndex = 7;
            this.btnxoahocki.Text = "Xoá";
            this.btnxoahocki.UseVisualStyleBackColor = true;
            this.btnxoahocki.Click += new System.EventHandler(this.btnxoahocki_Click);
            // 
            // button4
            // 
            this.button4.ForeColor = System.Drawing.Color.Blue;
            this.button4.Location = new System.Drawing.Point(382, 213);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 8;
            this.button4.Text = "Thoát";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Formhocki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gold;
            this.ClientSize = new System.Drawing.Size(515, 257);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnxoahocki);
            this.Controls.Add(this.btnsuahocki);
            this.Controls.Add(this.btnthemhocki);
            this.Controls.Add(this.dtghocki);
            this.Controls.Add(this.txtmamon);
            this.Controls.Add(this.txthocki);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "Formhocki";
            this.Text = "Học Kì";
            this.Load += new System.EventHandler(this.Formhocki_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtghocki)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txthocki;
        private System.Windows.Forms.TextBox txtmamon;
        private System.Windows.Forms.DataGridView dtghocki;
        private System.Windows.Forms.Button btnthemhocki;
        private System.Windows.Forms.Button btnsuahocki;
        private System.Windows.Forms.Button btnxoahocki;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridViewTextBoxColumn hocki;
        private System.Windows.Forms.DataGridViewTextBoxColumn mamon;
    }
}