﻿namespace GUI
{
    partial class Formreportdiemsv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formreportdiemsv));
            this.Table_KetQuaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataSetdiemsv = new GUI.DataSetdiemsv();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbreportdiem = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbhocki_diemsv = new System.Windows.Forms.ComboBox();
            this.Table_KetQuaTableAdapter = new GUI.DataSetdiemsvTableAdapters.Table_KetQuaTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.Table_KetQuaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetdiemsv)).BeginInit();
            this.SuspendLayout();
            // 
            // Table_KetQuaBindingSource
            // 
            this.Table_KetQuaBindingSource.DataMember = "Table_KetQua";
            this.Table_KetQuaBindingSource.DataSource = this.DataSetdiemsv;
            // 
            // DataSetdiemsv
            // 
            this.DataSetdiemsv.DataSetName = "DataSetdiemsv";
            this.DataSetdiemsv.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.Table_KetQuaBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "GUI.Report2.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(1, 50);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(1027, 410);
            this.reportViewer1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(260, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã Lớp";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // cmbreportdiem
            // 
            this.cmbreportdiem.FormattingEnabled = true;
            this.cmbreportdiem.Location = new System.Drawing.Point(309, 12);
            this.cmbreportdiem.Name = "cmbreportdiem";
            this.cmbreportdiem.Size = new System.Drawing.Size(153, 21);
            this.cmbreportdiem.TabIndex = 2;
            this.cmbreportdiem.SelectedValueChanged += new System.EventHandler(this.cmbreportdiem_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Học Kì";
            // 
            // cmbhocki_diemsv
            // 
            this.cmbhocki_diemsv.FormattingEnabled = true;
            this.cmbhocki_diemsv.Location = new System.Drawing.Point(92, 12);
            this.cmbhocki_diemsv.Name = "cmbhocki_diemsv";
            this.cmbhocki_diemsv.Size = new System.Drawing.Size(147, 21);
            this.cmbhocki_diemsv.TabIndex = 3;
            this.cmbhocki_diemsv.SelectedIndexChanged += new System.EventHandler(this.cmbhocki_diemsv_SelectedIndexChanged);
            this.cmbhocki_diemsv.SelectedValueChanged += new System.EventHandler(this.cmbhocki_diemsv_SelectedValueChanged);
            // 
            // Table_KetQuaTableAdapter
            // 
            this.Table_KetQuaTableAdapter.ClearBeforeFill = true;
            // 
            // Formreportdiemsv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 464);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbhocki_diemsv);
            this.Controls.Add(this.cmbreportdiem);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formreportdiemsv";
            this.Text = "Điểm Sinh Viên";
            this.Load += new System.EventHandler(this.Formreportdiemsv_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Table_KetQuaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetdiemsv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource Table_KetQuaBindingSource;
        private DataSetdiemsv DataSetdiemsv;
        private DataSetdiemsvTableAdapters.Table_KetQuaTableAdapter Table_KetQuaTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbreportdiem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbhocki_diemsv;
    }
}