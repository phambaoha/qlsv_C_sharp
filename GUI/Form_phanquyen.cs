﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;
namespace GUI
{
    public partial class Form_phanquyen : Form
    {
       public static DataTable dt = new DataTable();
        public Form_phanquyen()
        {
            InitializeComponent();
        }

        private void Form_phanquyen_Load(object sender, EventArgs e)
        {
            dt = BLL_LOGIN.loadthongtinformphanquyen();
            dtgphanquyen.DataSource = dt;
           
        }
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dtgphanquyen_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dtgphanquyen.CurrentRow.Index;
            txtmadn.Text = dtgphanquyen[0, index].Value.ToString();
            txtmatkhau.Text = dtgphanquyen[1, index].Value.ToString();
            cmbphanquyen.Text = dtgphanquyen[2, index].Value.ToString();
        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            try
            {

            DTO_LOGIN dtlogin = new DTO_LOGIN(txtmadn.Text, txtmatkhau.Text, cmbphanquyen.Text);
            BLL_LOGIN.themphanquyen(dtlogin);
            dtgphanquyen.DataSource = BLL_LOGIN.loadthongtinformphanquyen();

            txtmatkhau.ResetText();
            cmbphanquyen.ResetText();
            txtmadn.ResetText();
            txtmadn.Focus();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
            
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            DTO_LOGIN dtlogin = new DTO_LOGIN(txtmadn.Text, txtmatkhau.Text, cmbphanquyen.Text);
            BLL_LOGIN.suaphanquyen(dtlogin);
            dtgphanquyen.DataSource = BLL_LOGIN.loadthongtinformphanquyen();
            txtmatkhau.ResetText();
            cmbphanquyen.ResetText();
            txtmadn.ResetText();
            txtmadn.Focus();
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            DTO_LOGIN dtlogin = new DTO_LOGIN(txtmadn.Text, txtmatkhau.Text);
            BLL_LOGIN.xoaphanquyen(dtlogin);
            dtgphanquyen.DataSource = BLL_LOGIN.loadthongtinformphanquyen();
                txtmatkhau.ResetText();
            cmbphanquyen.ResetText();
            txtmadn.ResetText();
            txtmadn.Focus();
        }
    }
}
