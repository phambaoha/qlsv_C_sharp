﻿namespace GUI
{
    partial class txtimahepath
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(txtimahepath));
            this.dtgSINHVIEN = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dangxuattoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suathongtinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phanquyentoolstripmenuitem = new System.Windows.Forms.ToolStripMenuItem();
            this.cậpNhậtĐiểmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xEMĐIỂMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lớpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.họcKìToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mônHọcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.khoaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thôngTinSinhViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.điểmSinhViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.liênHệToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtmasinhvien = new System.Windows.Forms.TextBox();
            this.txthoten = new System.Windows.Forms.TextBox();
            this.txtdiachi = new System.Windows.Forms.TextBox();
            this.txtmalop = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker_ngaysinh = new System.Windows.Forms.DateTimePicker();
            this.cmbgioitinh = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnxoa = new System.Windows.Forms.Button();
            this.btnsua = new System.Windows.Forms.Button();
            this.btnthem = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.labelxinchao = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtimagepath = new System.Windows.Forms.TextBox();
            this.masv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hoten = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NGAYSINH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GIOITINH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DIACHI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MALOP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hinhanh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgSINHVIEN)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgSINHVIEN
            // 
            this.dtgSINHVIEN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtgSINHVIEN.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dtgSINHVIEN.ColumnHeadersHeight = 25;
            this.dtgSINHVIEN.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.masv,
            this.hoten,
            this.NGAYSINH,
            this.GIOITINH,
            this.DIACHI,
            this.MALOP,
            this.hinhanh});
            this.dtgSINHVIEN.Location = new System.Drawing.Point(29, 239);
            this.dtgSINHVIEN.Name = "dtgSINHVIEN";
            this.dtgSINHVIEN.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dtgSINHVIEN.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgSINHVIEN.Size = new System.Drawing.Size(1004, 389);
            this.dtgSINHVIEN.TabIndex = 0;
            this.dtgSINHVIEN.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowDrop = true;
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 5);
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.cậpNhậtĐiểmToolStripMenuItem,
            this.xEMĐIỂMToolStripMenuItem,
            this.quaToolStripMenuItem,
            this.thốngKêToolStripMenuItem,
            this.báoCáoToolStripMenuItem,
            this.liênHệToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1182, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dangxuattoolStripMenuItem,
            this.suathongtinToolStripMenuItem,
            this.phanquyentoolstripmenuitem});
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.DarkRed;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(72, 20);
            this.toolStripMenuItem1.Text = "Tài Khoản";
            this.toolStripMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // dangxuattoolStripMenuItem
            // 
            this.dangxuattoolStripMenuItem.ForeColor = System.Drawing.Color.DarkRed;
            this.dangxuattoolStripMenuItem.Name = "dangxuattoolStripMenuItem";
            this.dangxuattoolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.E)));
            this.dangxuattoolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.dangxuattoolStripMenuItem.Text = "Đăng Xuất";
            this.dangxuattoolStripMenuItem.Click += new System.EventHandler(this.đĂNGXUẤTToolStripMenuItem_Click);
            // 
            // suathongtinToolStripMenuItem
            // 
            this.suathongtinToolStripMenuItem.ForeColor = System.Drawing.Color.DarkRed;
            this.suathongtinToolStripMenuItem.Name = "suathongtinToolStripMenuItem";
            this.suathongtinToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this.suathongtinToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.suathongtinToolStripMenuItem.Text = "Đổi Mật Khẩu";
            this.suathongtinToolStripMenuItem.Click += new System.EventHandler(this.thêmTàiKhoảnToolStripMenuItem_Click);
            // 
            // phanquyentoolstripmenuitem
            // 
            this.phanquyentoolstripmenuitem.ForeColor = System.Drawing.Color.DarkRed;
            this.phanquyentoolstripmenuitem.Name = "phanquyentoolstripmenuitem";
            this.phanquyentoolstripmenuitem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.P)));
            this.phanquyentoolstripmenuitem.Size = new System.Drawing.Size(218, 22);
            this.phanquyentoolstripmenuitem.Text = "Phân Quyền";
            this.phanquyentoolstripmenuitem.Click += new System.EventHandler(this.phânQuyềnToolStripMenuItem_Click);
            // 
            // cậpNhậtĐiểmToolStripMenuItem
            // 
            this.cậpNhậtĐiểmToolStripMenuItem.ForeColor = System.Drawing.Color.DarkRed;
            this.cậpNhậtĐiểmToolStripMenuItem.Name = "cậpNhậtĐiểmToolStripMenuItem";
            this.cậpNhậtĐiểmToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.cậpNhậtĐiểmToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.cậpNhậtĐiểmToolStripMenuItem.Size = new System.Drawing.Size(103, 20);
            this.cậpNhậtĐiểmToolStripMenuItem.Text = "Cập Nhật Điểm ";
            this.cậpNhậtĐiểmToolStripMenuItem.ToolTipText = "CTRL+SHIFT+C";
            this.cậpNhậtĐiểmToolStripMenuItem.Click += new System.EventHandler(this.cậpNhậtĐiểmToolStripMenuItem_Click);
            // 
            // xEMĐIỂMToolStripMenuItem
            // 
            this.xEMĐIỂMToolStripMenuItem.Checked = true;
            this.xEMĐIỂMToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.xEMĐIỂMToolStripMenuItem.ForeColor = System.Drawing.Color.DarkRed;
            this.xEMĐIỂMToolStripMenuItem.Name = "xEMĐIỂMToolStripMenuItem";
            this.xEMĐIỂMToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.X)));
            this.xEMĐIỂMToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.xEMĐIỂMToolStripMenuItem.Text = "Xem Điểm";
            this.xEMĐIỂMToolStripMenuItem.ToolTipText = "Ctrl+Shift+X";
            this.xEMĐIỂMToolStripMenuItem.Click += new System.EventHandler(this.xEMĐIỂMToolStripMenuItem_Click);
            // 
            // quaToolStripMenuItem
            // 
            this.quaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lớpToolStripMenuItem,
            this.họcKìToolStripMenuItem,
            this.mônHọcToolStripMenuItem,
            this.khoaToolStripMenuItem});
            this.quaToolStripMenuItem.ForeColor = System.Drawing.Color.Maroon;
            this.quaToolStripMenuItem.Name = "quaToolStripMenuItem";
            this.quaToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.quaToolStripMenuItem.Text = "Quản Lí";
            // 
            // lớpToolStripMenuItem
            // 
            this.lớpToolStripMenuItem.ForeColor = System.Drawing.Color.Maroon;
            this.lớpToolStripMenuItem.Name = "lớpToolStripMenuItem";
            this.lớpToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.lớpToolStripMenuItem.Text = "Lớp";
            this.lớpToolStripMenuItem.Click += new System.EventHandler(this.lớpToolStripMenuItem_Click);
            // 
            // họcKìToolStripMenuItem
            // 
            this.họcKìToolStripMenuItem.ForeColor = System.Drawing.Color.Maroon;
            this.họcKìToolStripMenuItem.Name = "họcKìToolStripMenuItem";
            this.họcKìToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.họcKìToolStripMenuItem.Text = "Học kì";
            this.họcKìToolStripMenuItem.Click += new System.EventHandler(this.họcKìToolStripMenuItem_Click);
            // 
            // mônHọcToolStripMenuItem
            // 
            this.mônHọcToolStripMenuItem.ForeColor = System.Drawing.Color.Maroon;
            this.mônHọcToolStripMenuItem.Name = "mônHọcToolStripMenuItem";
            this.mônHọcToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.mônHọcToolStripMenuItem.Text = "Môn Học";
            this.mônHọcToolStripMenuItem.Click += new System.EventHandler(this.mônHọcToolStripMenuItem_Click);
            // 
            // khoaToolStripMenuItem
            // 
            this.khoaToolStripMenuItem.ForeColor = System.Drawing.Color.DarkRed;
            this.khoaToolStripMenuItem.Name = "khoaToolStripMenuItem";
            this.khoaToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.khoaToolStripMenuItem.Text = "Khoa";
            this.khoaToolStripMenuItem.Click += new System.EventHandler(this.khoaToolStripMenuItem_Click);
            // 
            // thốngKêToolStripMenuItem
            // 
            this.thốngKêToolStripMenuItem.ForeColor = System.Drawing.Color.DarkRed;
            this.thốngKêToolStripMenuItem.Name = "thốngKêToolStripMenuItem";
            this.thốngKêToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.thốngKêToolStripMenuItem.Text = "Thống Kê";
            this.thốngKêToolStripMenuItem.Click += new System.EventHandler(this.thốngKêToolStripMenuItem_Click);
            // 
            // báoCáoToolStripMenuItem
            // 
            this.báoCáoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thôngTinSinhViênToolStripMenuItem,
            this.điểmSinhViênToolStripMenuItem});
            this.báoCáoToolStripMenuItem.ForeColor = System.Drawing.Color.DarkRed;
            this.báoCáoToolStripMenuItem.Name = "báoCáoToolStripMenuItem";
            this.báoCáoToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.báoCáoToolStripMenuItem.Text = "Báo Cáo";
            // 
            // thôngTinSinhViênToolStripMenuItem
            // 
            this.thôngTinSinhViênToolStripMenuItem.ForeColor = System.Drawing.Color.DarkRed;
            this.thôngTinSinhViênToolStripMenuItem.Name = "thôngTinSinhViênToolStripMenuItem";
            this.thôngTinSinhViênToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.thôngTinSinhViênToolStripMenuItem.Text = "Thông Tin Sinh Viên";
            this.thôngTinSinhViênToolStripMenuItem.Click += new System.EventHandler(this.thôngTinSinhViênToolStripMenuItem_Click);
            // 
            // điểmSinhViênToolStripMenuItem
            // 
            this.điểmSinhViênToolStripMenuItem.ForeColor = System.Drawing.Color.DarkRed;
            this.điểmSinhViênToolStripMenuItem.Name = "điểmSinhViênToolStripMenuItem";
            this.điểmSinhViênToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.điểmSinhViênToolStripMenuItem.Text = "Điểm Sinh Viên";
            this.điểmSinhViênToolStripMenuItem.Click += new System.EventHandler(this.điểmSinhViênToolStripMenuItem_Click);
            // 
            // liênHệToolStripMenuItem
            // 
            this.liênHệToolStripMenuItem.ForeColor = System.Drawing.Color.DarkRed;
            this.liênHệToolStripMenuItem.Name = "liênHệToolStripMenuItem";
            this.liênHệToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.liênHệToolStripMenuItem.Text = "Liên hệ";
            this.liênHệToolStripMenuItem.Click += new System.EventHandler(this.liênHệToolStripMenuItem_Click);
            // 
            // txtmasinhvien
            // 
            this.txtmasinhvien.Location = new System.Drawing.Point(125, 54);
            this.txtmasinhvien.Name = "txtmasinhvien";
            this.txtmasinhvien.Size = new System.Drawing.Size(185, 20);
            this.txtmasinhvien.TabIndex = 6;
            this.txtmasinhvien.TextChanged += new System.EventHandler(this.txtmasinhvien_TextChanged);
            // 
            // txthoten
            // 
            this.txthoten.Location = new System.Drawing.Point(498, 60);
            this.txthoten.Multiline = true;
            this.txthoten.Name = "txthoten";
            this.txthoten.Size = new System.Drawing.Size(171, 23);
            this.txthoten.TabIndex = 7;
            // 
            // txtdiachi
            // 
            this.txtdiachi.AcceptsReturn = true;
            this.txtdiachi.Location = new System.Drawing.Point(125, 171);
            this.txtdiachi.Name = "txtdiachi";
            this.txtdiachi.Size = new System.Drawing.Size(185, 20);
            this.txtdiachi.TabIndex = 8;
            // 
            // txtmalop
            // 
            this.txtmalop.Location = new System.Drawing.Point(498, 176);
            this.txtmalop.Name = "txtmalop";
            this.txtmalop.Size = new System.Drawing.Size(171, 20);
            this.txtmalop.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(26, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Mã Sinh Viên";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(399, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Họ Tên";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(399, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Giới Tính";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label5.Location = new System.Drawing.Point(26, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Ngày Sinh";
            // 
            // dateTimePicker_ngaysinh
            // 
            this.dateTimePicker_ngaysinh.AllowDrop = true;
            this.dateTimePicker_ngaysinh.CustomFormat = "";
            this.dateTimePicker_ngaysinh.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_ngaysinh.Location = new System.Drawing.Point(125, 109);
            this.dateTimePicker_ngaysinh.Name = "dateTimePicker_ngaysinh";
            this.dateTimePicker_ngaysinh.ShowUpDown = true;
            this.dateTimePicker_ngaysinh.Size = new System.Drawing.Size(185, 20);
            this.dateTimePicker_ngaysinh.TabIndex = 16;
            this.dateTimePicker_ngaysinh.Value = new System.DateTime(2017, 4, 30, 0, 0, 0, 0);
            this.dateTimePicker_ngaysinh.ValueChanged += new System.EventHandler(this.dateTimePicker_ngaysinh_ValueChanged);
            // 
            // cmbgioitinh
            // 
            this.cmbgioitinh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbgioitinh.FormattingEnabled = true;
            this.cmbgioitinh.Items.AddRange(new object[] {
            "Nam",
            "Nữ"});
            this.cmbgioitinh.Location = new System.Drawing.Point(498, 114);
            this.cmbgioitinh.Name = "cmbgioitinh";
            this.cmbgioitinh.Size = new System.Drawing.Size(171, 21);
            this.cmbgioitinh.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(26, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Địa Chỉ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(399, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Mã Lớp";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 100;
            this.toolTip1.AutoPopDelay = 4000;
            this.toolTip1.InitialDelay = 100;
            this.toolTip1.ReshowDelay = 20;
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup_1);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::GUI.Properties.Resources.search;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.Location = new System.Drawing.Point(686, 176);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(30, 21);
            this.button2.TabIndex = 29;
            this.toolTip1.SetToolTip(this.button2, "Tìm Mã SV");
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::GUI.Properties.Resources.search;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(325, 54);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 22);
            this.button1.TabIndex = 28;
            this.toolTip1.SetToolTip(this.button1, "Tìm Mã SV");
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::GUI.Properties.Resources.stock_vector_reload_icon_424480606;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Location = new System.Drawing.Point(1093, 337);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(77, 73);
            this.button3.TabIndex = 24;
            this.toolTip1.SetToolTip(this.button3, "Reset Dữ Liệu");
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnxoa
            // 
            this.btnxoa.BackColor = System.Drawing.Color.White;
            this.btnxoa.BackgroundImage = global::GUI.Properties.Resources.Button_Close_icon;
            this.btnxoa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnxoa.Location = new System.Drawing.Point(1093, 239);
            this.btnxoa.Name = "btnxoa";
            this.btnxoa.Size = new System.Drawing.Size(75, 75);
            this.btnxoa.TabIndex = 3;
            this.toolTip1.SetToolTip(this.btnxoa, "Xoá Dữ Liệu");
            this.btnxoa.UseVisualStyleBackColor = false;
            this.btnxoa.Click += new System.EventHandler(this.btnxoa_Click);
            // 
            // btnsua
            // 
            this.btnsua.BackColor = System.Drawing.Color.White;
            this.btnsua.BackgroundImage = global::GUI.Properties.Resources.edit_validated_icon;
            this.btnsua.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsua.Location = new System.Drawing.Point(1090, 141);
            this.btnsua.Name = "btnsua";
            this.btnsua.Size = new System.Drawing.Size(77, 75);
            this.btnsua.TabIndex = 2;
            this.toolTip1.SetToolTip(this.btnsua, "Sửa Dữ Liệu");
            this.btnsua.UseVisualStyleBackColor = false;
            this.btnsua.Click += new System.EventHandler(this.btnsua_Click);
            // 
            // btnthem
            // 
            this.btnthem.BackColor = System.Drawing.Color.White;
            this.btnthem.BackgroundImage = global::GUI.Properties.Resources.add_iconbig__1_;
            this.btnthem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnthem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnthem.Location = new System.Drawing.Point(1091, 47);
            this.btnthem.Name = "btnthem";
            this.btnthem.Size = new System.Drawing.Size(76, 77);
            this.btnthem.TabIndex = 1;
            this.btnthem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.btnthem, "Thêm Dữ Liệu");
            this.btnthem.UseVisualStyleBackColor = false;
            this.btnthem.Click += new System.EventHandler(this.btnthem_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(712, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Chọn ảnh";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(753, 203);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(91, 17);
            this.checkBox1.TabIndex = 27;
            this.checkBox1.Text = "ảnh mặc định";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(750, 47);
            this.label9.MaximumSize = new System.Drawing.Size(5, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(5, 1);
            this.label9.TabIndex = 30;
            this.label9.Text = "label9";
            // 
            // labelxinchao
            // 
            this.labelxinchao.AutoSize = true;
            this.labelxinchao.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelxinchao.ForeColor = System.Drawing.Color.Red;
            this.labelxinchao.Location = new System.Drawing.Point(811, 0);
            this.labelxinchao.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.labelxinchao.Name = "labelxinchao";
            this.labelxinchao.Size = new System.Drawing.Size(0, 24);
            this.labelxinchao.TabIndex = 31;
            this.labelxinchao.Tag = "";
            this.labelxinchao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::GUI.Properties.Resources.avatar;
            this.pictureBox1.Location = new System.Drawing.Point(815, 54);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(218, 143);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // txtimagepath
            // 
            this.txtimagepath.Location = new System.Drawing.Point(866, 203);
            this.txtimagepath.Name = "txtimagepath";
            this.txtimagepath.Size = new System.Drawing.Size(120, 20);
            this.txtimagepath.TabIndex = 25;
            this.txtimagepath.TextChanged += new System.EventHandler(this.txtimagepath_TextChanged);
            // 
            // masv
            // 
            this.masv.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.masv.DataPropertyName = "masv";
            this.masv.HeaderText = "Mã Sinh Viên";
            this.masv.Name = "masv";
            // 
            // hoten
            // 
            this.hoten.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.hoten.DataPropertyName = "hoten";
            this.hoten.HeaderText = "Họ Tên";
            this.hoten.Name = "hoten";
            // 
            // NGAYSINH
            // 
            this.NGAYSINH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NGAYSINH.DataPropertyName = "NGAYSINH";
            this.NGAYSINH.HeaderText = "Ngày Sinh";
            this.NGAYSINH.Name = "NGAYSINH";
            // 
            // GIOITINH
            // 
            this.GIOITINH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.GIOITINH.DataPropertyName = "GIOITINH";
            this.GIOITINH.HeaderText = "Giới Tính";
            this.GIOITINH.Name = "GIOITINH";
            // 
            // DIACHI
            // 
            this.DIACHI.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DIACHI.DataPropertyName = "DIACHI";
            this.DIACHI.HeaderText = "Địa Chỉ";
            this.DIACHI.Name = "DIACHI";
            // 
            // MALOP
            // 
            this.MALOP.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MALOP.DataPropertyName = "MALOP";
            this.MALOP.HeaderText = "Mã Lớp";
            this.MALOP.Name = "MALOP";
            // 
            // hinhanh
            // 
            this.hinhanh.DataPropertyName = "hinhanh";
            this.hinhanh.HeaderText = "Hinh anh";
            this.hinhanh.Name = "hinhanh";
            // 
            // txtimahepath
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1182, 640);
            this.Controls.Add(this.labelxinchao);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtimagepath);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbgioitinh);
            this.Controls.Add(this.dateTimePicker_ngaysinh);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtmalop);
            this.Controls.Add(this.txtdiachi);
            this.Controls.Add(this.txthoten);
            this.Controls.Add(this.txtmasinhvien);
            this.Controls.Add(this.btnxoa);
            this.Controls.Add(this.btnsua);
            this.Controls.Add(this.btnthem);
            this.Controls.Add(this.dtgSINHVIEN);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "txtimahepath";
            this.Text = "Chương Trình Quản Lí Điểm Sinh Viên";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgSINHVIEN)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgSINHVIEN;
        private System.Windows.Forms.Button btnthem;
        private System.Windows.Forms.Button btnsua;
        private System.Windows.Forms.Button btnxoa;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.TextBox txtmasinhvien;
        private System.Windows.Forms.TextBox txthoten;
        private System.Windows.Forms.TextBox txtdiachi;
        private System.Windows.Forms.TextBox txtmalop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker_ngaysinh;
        private System.Windows.Forms.ComboBox cmbgioitinh;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripMenuItem cậpNhậtĐiểmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xEMĐIỂMToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem quaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lớpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem họcKìToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mônHọcToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ToolStripMenuItem khoaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thôngTinSinhViênToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem điểmSinhViênToolStripMenuItem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripMenuItem dangxuattoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suathongtinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phanquyentoolstripmenuitem;
        private System.Windows.Forms.Label labelxinchao;
        private System.Windows.Forms.ToolStripMenuItem thốngKêToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem liênHệToolStripMenuItem;
        private System.Windows.Forms.TextBox txtimagepath;
        private System.Windows.Forms.DataGridViewTextBoxColumn masv;
        private System.Windows.Forms.DataGridViewTextBoxColumn hoten;
        private System.Windows.Forms.DataGridViewTextBoxColumn NGAYSINH;
        private System.Windows.Forms.DataGridViewTextBoxColumn GIOITINH;
        private System.Windows.Forms.DataGridViewTextBoxColumn DIACHI;
        private System.Windows.Forms.DataGridViewTextBoxColumn MALOP;
        private System.Windows.Forms.DataGridViewTextBoxColumn hinhanh;
    }
}