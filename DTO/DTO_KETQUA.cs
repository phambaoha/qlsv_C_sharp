﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_KETQUA
    {
        string _Masv;

        public string Masv
        {
            get { return _Masv; }
            set { _Masv = value; }
        }
        string _hoten;

        public string Hoten
        {
            get { return _hoten; }
            set { _hoten = value; }
        }
        private float _Diemmonhoc;

        public float Diemmonhoc
        {
            get { return _Diemmonhoc; }
            set { _Diemmonhoc = value; }
        }
        private string _hocki;

        public string Hocki
        {
            get { return _hocki; }
            set { _hocki = value; }
        }
        private string _malop;

        public string Malop
        {
            get { return _malop; }
            set { _malop = value; }
        }
        string _mamon;

        public string Mamon
        {
            get { return _mamon; }
            set { _mamon = value; }
        }
          int _diemrenluyen;

public int Diemrenluyen
{
  get { return _diemrenluyen; }
  set { _diemrenluyen = value; }
}

        private float _diema;

public float Diema
{
  get { return _diema; }
  set { _diema = value; }
}
         float _diemb;

public float Diemb
{
  get { return _diemb; }
  set { _diemb = value; }
}
        float _diemc;

public float Diemc
{
  get { return _diemc; }
  set { _diemc = value; }
}
        string _diemch;

public string Diemch
{
  get { return _diemch; }
  set { _diemch = value; }
}
        public DTO_KETQUA(string thocki)
        {
            _hocki = thocki;
        }

        float _heso;

        public float Heso
        {
            get { return _heso; }
            set { _heso = value; }
        }
        int _sotc;

        public int Sotc
        {
            get { return _sotc; }
            set { _sotc = value; }
        }
        float _diemtonghebon;

        public float Diemtonghebon
        {
            get { return _diemtonghebon; }
            set { _diemtonghebon = value; }
        }
      public DTO_KETQUA(string tmasv, string thoten, string thocki, string tmamon,string tmalop,float tdiema, float tdiemb, float tdiemc,float tdiemmonhoc,string tdiemchu, float theso,int tdiemrenluyen,int tsotc, float tdiemtonghebon)
        {
            _Masv = tmasv;
            _hoten = thoten;
            _hocki = thocki;
            _mamon = tmamon;
            _malop = tmalop;
            _diema = tdiema;
            _diemb = tdiemb;
            _diemc = tdiemc;
            _Diemmonhoc = tdiemmonhoc;
            _diemch = tdiemchu;
            _heso = theso;
            _diemrenluyen = tdiemrenluyen;
            _sotc = tsotc;
            _diemtonghebon = tdiemtonghebon;
        }
        public DTO_KETQUA(string tmasv, string tmamon)
      {
          _Masv = tmasv;
          _mamon = tmamon;
      } 
        
    }
}
