﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
using System.Data;
namespace BLL
{
    public class BLL_KETQUA
    {
        public static DataTable loaddlcomboboxhocki()
        {
            return DAL_KETQUA.loaddlcomboboxhocki();
        }
        //public static DataTable locdltheohocki(DTO_KETQUA kq)
        //{
        //    return DAL_KETQUA.locdltheohocki(kq);
        //} 
        public static void themdiemsv(DTO_KETQUA kq)
        {
            DAL_KETQUA.themdiemsv(kq);
        }
        public static void Xoadiemsv(DTO_KETQUA kq)
        {
            DAL_KETQUA.Xoadiemsv(kq);
        }
        public static void suadiemsv(DTO_KETQUA kq)
        {
            DAL_KETQUA.suadiemsv(kq);
        }
        public static DataTable loaddldiem(DTO_SINHVIEN sv, DTO_HOCKI hk)
        {
            return DAL_KETQUA.loaddldiem(sv, hk);
        }
        public static DataTable xemtoanfullhocki(DTO_SINHVIEN sv)
        {
            return DAL_KETQUA.xemtoanfullhocki(sv);
        }
        public static DataTable diemtong1ki(DTO_SINHVIEN sv, DTO_HOCKI hk)
        {
            return DAL_KETQUA.diemtong1ki(sv, hk);
        }
        public static void updatetable_ketqua()
        {
            DAL_KETQUA.updatetable_ketqua();
        }
       
        public static DataTable timkiemmalop(DTO_LOP lp)
        {
            return DAL_KETQUA.timkiemmalop(lp);
        }
        public static DataTable timkiemhocki(DTO_HOCKI hk)
        { return DAL_KETQUA.timkiemhocki(hk); }
        public static DataTable timkiemmamon(DTO_MONHOC mh)
        {
            return DAL_KETQUA.timkiemmamon(mh);
        }
        public static DataTable timkiemmasv(DTO_SINHVIEN sv)
        {
            return DAL_KETQUA.timkiemmasv(sv);
        }
       
    }
}
