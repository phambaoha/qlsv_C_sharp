﻿namespace GUI
{
    partial class cmbtenmon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cmbtenmon));
            this.cmbhocki = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtgcapnhatdiemsv = new System.Windows.Forms.DataGridView();
            this.masv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hoten = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hocki = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mamon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.malop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diema = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diemb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diemc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diemmonhoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diemchu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diemrenluyen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sotc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diemtonghebon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbmamon = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txthoten = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.cmbmakhoa = new System.Windows.Forms.Label();
            this.txtsotc = new System.Windows.Forms.TextBox();
            this.cmbmalop = new System.Windows.Forms.ComboBox();
            this.cbmasv = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtdiemc = new System.Windows.Forms.TextBox();
            this.txtdiemb = new System.Windows.Forms.TextBox();
            this.txtdiema = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtdiemhe4 = new System.Windows.Forms.GroupBox();
            this.txtdiemtonghebon = new System.Windows.Forms.TextBox();
            this.txtheso = new System.Windows.Forms.TextBox();
            this.txtdiemtongmonhoc = new System.Windows.Forms.TextBox();
            this.txtdiemrenluyen = new System.Windows.Forms.TextBox();
            this.txtdiemchu1 = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnreset = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dtgcapnhatdiemsv)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.txtdiemhe4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbhocki
            // 
            this.cmbhocki.FormattingEnabled = true;
            this.cmbhocki.Location = new System.Drawing.Point(75, 43);
            this.cmbhocki.Name = "cmbhocki";
            this.cmbhocki.Size = new System.Drawing.Size(154, 21);
            this.cmbhocki.TabIndex = 0;
            this.cmbhocki.SelectedIndexChanged += new System.EventHandler(this.cmbhocki_SelectedIndexChanged);
            this.cmbhocki.SelectedValueChanged += new System.EventHandler(this.cmbhocki_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Học Kì";
            // 
            // dtgcapnhatdiemsv
            // 
            this.dtgcapnhatdiemsv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgcapnhatdiemsv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtgcapnhatdiemsv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgcapnhatdiemsv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.masv,
            this.hoten,
            this.hocki,
            this.mamon,
            this.malop,
            this.diema,
            this.diemb,
            this.diemc,
            this.diemmonhoc,
            this.diemchu,
            this.diemrenluyen,
            this.heso,
            this.sotc,
            this.diemtonghebon});
            this.dtgcapnhatdiemsv.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dtgcapnhatdiemsv.Location = new System.Drawing.Point(255, 281);
            this.dtgcapnhatdiemsv.Name = "dtgcapnhatdiemsv";
            this.dtgcapnhatdiemsv.Size = new System.Drawing.Size(1055, 247);
            this.dtgcapnhatdiemsv.TabIndex = 4;
            this.dtgcapnhatdiemsv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgcapnhatdiemsv_CellContentClick);
            // 
            // masv
            // 
            this.masv.DataPropertyName = "masv";
            this.masv.HeaderText = "Mã SV";
            this.masv.Name = "masv";
            // 
            // hoten
            // 
            this.hoten.DataPropertyName = "hoten";
            this.hoten.HeaderText = "Họ Tên";
            this.hoten.Name = "hoten";
            // 
            // hocki
            // 
            this.hocki.DataPropertyName = "hocki";
            this.hocki.HeaderText = "Học Kì";
            this.hocki.Name = "hocki";
            // 
            // mamon
            // 
            this.mamon.DataPropertyName = "mamon";
            this.mamon.HeaderText = "Mã Môn";
            this.mamon.Name = "mamon";
            // 
            // malop
            // 
            this.malop.DataPropertyName = "malop";
            this.malop.HeaderText = "Mã Lớp";
            this.malop.Name = "malop";
            // 
            // diema
            // 
            this.diema.DataPropertyName = "diema";
            this.diema.HeaderText = "Điểm A";
            this.diema.Name = "diema";
            // 
            // diemb
            // 
            this.diemb.DataPropertyName = "diemb";
            this.diemb.HeaderText = "Điểm B";
            this.diemb.Name = "diemb";
            // 
            // diemc
            // 
            this.diemc.DataPropertyName = "diemc";
            this.diemc.HeaderText = "Điểm C";
            this.diemc.Name = "diemc";
            // 
            // diemmonhoc
            // 
            this.diemmonhoc.DataPropertyName = "diemmonhoc";
            this.diemmonhoc.HeaderText = "Điểm Môn Học";
            this.diemmonhoc.Name = "diemmonhoc";
            // 
            // diemchu
            // 
            this.diemchu.DataPropertyName = "diemchu";
            this.diemchu.HeaderText = "Điểm Chữ";
            this.diemchu.Name = "diemchu";
            // 
            // diemrenluyen
            // 
            this.diemrenluyen.DataPropertyName = "diemrenluyen";
            this.diemrenluyen.HeaderText = "Điểm RL";
            this.diemrenluyen.Name = "diemrenluyen";
            // 
            // heso
            // 
            this.heso.DataPropertyName = "heso";
            this.heso.HeaderText = "heso";
            this.heso.Name = "heso";
            this.heso.Visible = false;
            // 
            // sotc
            // 
            this.sotc.DataPropertyName = "sotc";
            this.sotc.HeaderText = "sotc";
            this.sotc.Name = "sotc";
            this.sotc.Visible = false;
            // 
            // diemtonghebon
            // 
            this.diemtonghebon.DataPropertyName = "diemtonghebon";
            this.diemtonghebon.HeaderText = "diemtonghebon";
            this.diemtonghebon.Name = "diemtonghebon";
            this.diemtonghebon.Visible = false;
            // 
            // cmbmamon
            // 
            this.cmbmamon.FormattingEnabled = true;
            this.cmbmamon.Location = new System.Drawing.Point(75, 87);
            this.cmbmamon.Name = "cmbmamon";
            this.cmbmamon.Size = new System.Drawing.Size(154, 21);
            this.cmbmamon.TabIndex = 5;
            this.cmbmamon.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.cmbmamon.SelectedValueChanged += new System.EventHandler(this.cmbtenmonhoc_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "MÃ MÔN ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mã Lớp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(292, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Họ Tên";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(292, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Mã Sinh Viên";
            // 
            // txthoten
            // 
            this.txthoten.Location = new System.Drawing.Point(372, 93);
            this.txthoten.Name = "txthoten";
            this.txthoten.Size = new System.Drawing.Size(130, 20);
            this.txthoten.TabIndex = 12;
            this.txthoten.TextChanged += new System.EventHandler(this.txthoten_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.BackColor = System.Drawing.Color.Gold;
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.cmbmakhoa);
            this.groupBox1.Controls.Add(this.txtsotc);
            this.groupBox1.Controls.Add(this.cmbmalop);
            this.groupBox1.Controls.Add(this.cbmasv);
            this.groupBox1.Controls.Add(this.txthoten);
            this.groupBox1.Controls.Add(this.cmbmamon);
            this.groupBox1.Controls.Add(this.cmbhocki);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.ForeColor = System.Drawing.Color.Red;
            this.groupBox1.Location = new System.Drawing.Point(26, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(555, 231);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "LỌC ";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button9
            // 
            this.button9.BackgroundImage = global::GUI.Properties.Resources.search;
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button9.Location = new System.Drawing.Point(508, 38);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(30, 21);
            this.button9.TabIndex = 33;
            this.toolTip1.SetToolTip(this.button9, " Tìm theo mã sinh viên");
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.BackgroundImage = global::GUI.Properties.Resources.search;
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button8.Location = new System.Drawing.Point(235, 87);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(30, 21);
            this.button8.TabIndex = 32;
            this.toolTip1.SetToolTip(this.button8, " Tìm theo mã môn");
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.BackgroundImage = global::GUI.Properties.Resources.search;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.Location = new System.Drawing.Point(235, 43);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(30, 21);
            this.button7.TabIndex = 31;
            this.toolTip1.SetToolTip(this.button7, "Tìm theo học kì");
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackgroundImage = global::GUI.Properties.Resources.search;
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button6.Location = new System.Drawing.Point(235, 174);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(30, 21);
            this.button6.TabIndex = 30;
            this.toolTip1.SetToolTip(this.button6, " Tìm theo mã lớp");
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(75, 128);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(154, 21);
            this.comboBox1.TabIndex = 29;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_2);
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // cmbmakhoa
            // 
            this.cmbmakhoa.AutoSize = true;
            this.cmbmakhoa.Location = new System.Drawing.Point(6, 131);
            this.cmbmakhoa.Name = "cmbmakhoa";
            this.cmbmakhoa.Size = new System.Drawing.Size(50, 13);
            this.cmbmakhoa.TabIndex = 28;
            this.cmbmakhoa.Text = "Mã Khoa";
            // 
            // txtsotc
            // 
            this.txtsotc.Location = new System.Drawing.Point(145, 129);
            this.txtsotc.Name = "txtsotc";
            this.txtsotc.Size = new System.Drawing.Size(65, 20);
            this.txtsotc.TabIndex = 27;
            this.txtsotc.Visible = false;
            this.txtsotc.TextChanged += new System.EventHandler(this.txtsotc_TextChanged);
            // 
            // cmbmalop
            // 
            this.cmbmalop.FormattingEnabled = true;
            this.cmbmalop.Location = new System.Drawing.Point(75, 174);
            this.cmbmalop.Name = "cmbmalop";
            this.cmbmalop.Size = new System.Drawing.Size(154, 21);
            this.cmbmalop.TabIndex = 26;
            this.cmbmalop.SelectedIndexChanged += new System.EventHandler(this.cmbmalop_SelectedIndexChanged);
            this.cmbmalop.SelectedValueChanged += new System.EventHandler(this.cmbmalop_SelectedValueChanged);
            // 
            // cbmasv
            // 
            this.cbmasv.FormattingEnabled = true;
            this.cbmasv.Location = new System.Drawing.Point(372, 38);
            this.cbmasv.Name = "cbmasv";
            this.cbmasv.Size = new System.Drawing.Size(130, 21);
            this.cbmasv.TabIndex = 25;
            this.cbmasv.SelectedIndexChanged += new System.EventHandler(this.cbhoten_SelectedIndexChanged);
            this.cbmasv.SelectedValueChanged += new System.EventHandler(this.cbmasv_SelectedValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(83, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Điểm A";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(366, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Điểm Môn Học";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(83, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Điểm C";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(83, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Điểm B";
            // 
            // txtdiemc
            // 
            this.txtdiemc.Location = new System.Drawing.Point(167, 124);
            this.txtdiemc.MaxLength = 3;
            this.txtdiemc.Name = "txtdiemc";
            this.txtdiemc.Size = new System.Drawing.Size(130, 20);
            this.txtdiemc.TabIndex = 19;
            // 
            // txtdiemb
            // 
            this.txtdiemb.Location = new System.Drawing.Point(167, 71);
            this.txtdiemb.MaxLength = 3;
            this.txtdiemb.Name = "txtdiemb";
            this.txtdiemb.Size = new System.Drawing.Size(130, 20);
            this.txtdiemb.TabIndex = 20;
            // 
            // txtdiema
            // 
            this.txtdiema.Location = new System.Drawing.Point(167, 24);
            this.txtdiema.MaxLength = 3;
            this.txtdiema.Name = "txtdiema";
            this.txtdiema.Size = new System.Drawing.Size(130, 20);
            this.txtdiema.TabIndex = 21;
            this.txtdiema.TextChanged += new System.EventHandler(this.txtdiema_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(366, 131);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Điểm Rèn Luyện";
            // 
            // txtdiemhe4
            // 
            this.txtdiemhe4.BackColor = System.Drawing.Color.Gold;
            this.txtdiemhe4.Controls.Add(this.txtdiemtonghebon);
            this.txtdiemhe4.Controls.Add(this.txtheso);
            this.txtdiemhe4.Controls.Add(this.txtdiemtongmonhoc);
            this.txtdiemhe4.Controls.Add(this.txtdiemrenluyen);
            this.txtdiemhe4.Controls.Add(this.txtdiemchu1);
            this.txtdiemhe4.Controls.Add(this.button5);
            this.txtdiemhe4.Controls.Add(this.label11);
            this.txtdiemhe4.Controls.Add(this.label6);
            this.txtdiemhe4.Controls.Add(this.label9);
            this.txtdiemhe4.Controls.Add(this.txtdiema);
            this.txtdiemhe4.Controls.Add(this.label7);
            this.txtdiemhe4.Controls.Add(this.txtdiemb);
            this.txtdiemhe4.Controls.Add(this.label8);
            this.txtdiemhe4.Controls.Add(this.txtdiemc);
            this.txtdiemhe4.Controls.Add(this.label10);
            this.txtdiemhe4.ForeColor = System.Drawing.Color.Red;
            this.txtdiemhe4.Location = new System.Drawing.Point(602, 27);
            this.txtdiemhe4.Name = "txtdiemhe4";
            this.txtdiemhe4.Size = new System.Drawing.Size(708, 231);
            this.txtdiemhe4.TabIndex = 26;
            this.txtdiemhe4.TabStop = false;
            this.txtdiemhe4.Text = "Sửa Điểm";
            this.txtdiemhe4.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // txtdiemtonghebon
            // 
            this.txtdiemtonghebon.Location = new System.Drawing.Point(465, 170);
            this.txtdiemtonghebon.Name = "txtdiemtonghebon";
            this.txtdiemtonghebon.Size = new System.Drawing.Size(100, 20);
            this.txtdiemtonghebon.TabIndex = 33;
            this.txtdiemtonghebon.Visible = false;
            // 
            // txtheso
            // 
            this.txtheso.Location = new System.Drawing.Point(628, 31);
            this.txtheso.Name = "txtheso";
            this.txtheso.Size = new System.Drawing.Size(64, 20);
            this.txtheso.TabIndex = 32;
            this.txtheso.Visible = false;
            // 
            // txtdiemtongmonhoc
            // 
            this.txtdiemtongmonhoc.Location = new System.Drawing.Point(466, 75);
            this.txtdiemtongmonhoc.MaxLength = 4;
            this.txtdiemtongmonhoc.Name = "txtdiemtongmonhoc";
            this.txtdiemtongmonhoc.Size = new System.Drawing.Size(134, 20);
            this.txtdiemtongmonhoc.TabIndex = 31;
            this.txtdiemtongmonhoc.TextChanged += new System.EventHandler(this.txtdiemtongmonhoc_TextChanged);
            // 
            // txtdiemrenluyen
            // 
            this.txtdiemrenluyen.Location = new System.Drawing.Point(466, 121);
            this.txtdiemrenluyen.Name = "txtdiemrenluyen";
            this.txtdiemrenluyen.Size = new System.Drawing.Size(134, 20);
            this.txtdiemrenluyen.TabIndex = 30;
            // 
            // txtdiemchu1
            // 
            this.txtdiemchu1.AllowDrop = true;
            this.txtdiemchu1.Location = new System.Drawing.Point(465, 31);
            this.txtdiemchu1.Name = "txtdiemchu1";
            this.txtdiemchu1.Size = new System.Drawing.Size(135, 20);
            this.txtdiemchu1.TabIndex = 29;
            this.txtdiemchu1.TextChanged += new System.EventHandler(this.txtdiemchu1_TextChanged);
            // 
            // button5
            // 
            this.button5.ForeColor = System.Drawing.Color.Blue;
            this.button5.Location = new System.Drawing.Point(344, 177);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 28;
            this.button5.Text = "Tính Điểm";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(366, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Điểm Chữ";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Blue;
            this.button1.Location = new System.Drawing.Point(55, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 27);
            this.button1.TabIndex = 27;
            this.button1.Text = "Thêm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Gold;
            this.groupBox2.Controls.Add(this.btnreset);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(26, 281);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(194, 247);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thao Tác";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // btnreset
            // 
            this.btnreset.Location = new System.Drawing.Point(55, 155);
            this.btnreset.Name = "btnreset";
            this.btnreset.Size = new System.Drawing.Size(75, 26);
            this.btnreset.TabIndex = 31;
            this.btnreset.Text = "Reset";
            this.btnreset.UseVisualStyleBackColor = true;
            this.btnreset.Click += new System.EventHandler(this.btnreset_Click);
            // 
            // button4
            // 
            this.button4.ForeColor = System.Drawing.Color.Blue;
            this.button4.Location = new System.Drawing.Point(55, 200);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 27);
            this.button4.TabIndex = 30;
            this.button4.Text = "Thoát";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.Color.Blue;
            this.button3.Location = new System.Drawing.Point(55, 65);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 27);
            this.button3.TabIndex = 29;
            this.button3.Text = "Sửa";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.Color.Blue;
            this.button2.Location = new System.Drawing.Point(55, 111);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 27);
            this.button2.TabIndex = 28;
            this.button2.Text = "Xoá";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // cmbtenmon
            // 
            this.AcceptButton = this.button5;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1325, 551);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtdiemhe4);
            this.Controls.Add(this.dtgcapnhatdiemsv);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "cmbtenmon";
            this.Text = "Cập Nhật Điểm Sinh Viên";
            this.Load += new System.EventHandler(this.Form_capnhatdiemsinhvien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgcapnhatdiemsv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.txtdiemhe4.ResumeLayout(false);
            this.txtdiemhe4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbhocki;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dtgcapnhatdiemsv;
        private System.Windows.Forms.ComboBox cmbmamon;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txthoten;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtdiemc;
        private System.Windows.Forms.TextBox txtdiemb;
        private System.Windows.Forms.TextBox txtdiema;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox txtdiemhe4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox cbmasv;
        private System.Windows.Forms.ComboBox cmbmalop;
        private System.Windows.Forms.TextBox txtdiemtongmonhoc;
        private System.Windows.Forms.TextBox txtdiemrenluyen;
        private System.Windows.Forms.TextBox txtdiemchu1;
        private System.Windows.Forms.TextBox txtheso;
        private System.Windows.Forms.TextBox txtsotc;
        private System.Windows.Forms.TextBox txtdiemtonghebon;
        private System.Windows.Forms.Label cmbmakhoa;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn masv;
        private System.Windows.Forms.DataGridViewTextBoxColumn hoten;
        private System.Windows.Forms.DataGridViewTextBoxColumn hocki;
        private System.Windows.Forms.DataGridViewTextBoxColumn mamon;
        private System.Windows.Forms.DataGridViewTextBoxColumn malop;
        private System.Windows.Forms.DataGridViewTextBoxColumn diema;
        private System.Windows.Forms.DataGridViewTextBoxColumn diemb;
        private System.Windows.Forms.DataGridViewTextBoxColumn diemc;
        private System.Windows.Forms.DataGridViewTextBoxColumn diemmonhoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn diemchu;
        private System.Windows.Forms.DataGridViewTextBoxColumn diemrenluyen;
        private System.Windows.Forms.DataGridViewTextBoxColumn heso;
        private System.Windows.Forms.DataGridViewTextBoxColumn sotc;
        private System.Windows.Forms.DataGridViewTextBoxColumn diemtonghebon;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btnreset;
    }
}