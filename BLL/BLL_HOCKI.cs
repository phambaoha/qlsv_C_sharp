﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;
using System.Data;
namespace BLL
{
    public class BLL_HOCKI
    {
        public static DataTable loadhocki()
        {
            return DAL_HOCKI.loadhocki();
        }
        public static DataTable loaddlcmbmamon(DTO_HOCKI hk)
        {
            return DAL_HOCKI.loaddlcmbmamon(hk);
        }
        public static DataTable loaddlcmbhocki()
        {
            return DAL_HOCKI.loaddlcmbhocki();
        }
        public static void themhocki(DTO_HOCKI hk)
        {
            DAL_HOCKI.themhocki(hk);
        }
        public static void suahocki(DTO_HOCKI hk)
        {
            DAL_HOCKI.suahocki(hk);
        }
        public static void xoahocki(DTO_HOCKI hk)
        {
            DAL_HOCKI.xoahocki(hk);
        }
        public static DataTable hocbongsinhvien(DTO_HOCKI hk)
        {
            return DAL_HOCKI.hocbongsinhvien(hk);
        }
        public static DataTable canhcaosinhvien(DTO_HOCKI hk)
        {
            return DAL_HOCKI.canhcaosinhvien(hk);
        }
    }
}
