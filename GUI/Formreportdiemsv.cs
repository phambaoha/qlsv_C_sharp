﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;

namespace GUI
{
    public partial class Formreportdiemsv : Form
    {
        public Formreportdiemsv()
        {
            InitializeComponent();
        }

        private void Formreportdiemsv_Load(object sender, EventArgs e)
        {
            
            cmbhocki_diemsv.DataSource = BLL_HOCKI.loaddlcmbhocki();
            cmbhocki_diemsv.DisplayMember = "hocki";
            cmbhocki_diemsv.ValueMember = "hocki";

            cmbreportdiem.DataSource = BLL_LOP.loaddlcomboxmalop();
            cmbreportdiem.DisplayMember = "malop";
            cmbreportdiem.ValueMember = "malop";
            // TODO: This line of code loads data into the 'DataSetdiemsv.Table_KetQua' table. You can move, or remove it, as needed.
            //this.Table_KetQuaTableAdapter.Fill(this.DataSetdiemsv.Table_KetQua);

          //  this.reportViewer1.RefreshReport();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cmbreportdiem_SelectedValueChanged(object sender, EventArgs e)
        {
              string hocki = cmbhocki_diemsv.SelectedValue.ToString().Trim();
              string malop = cmbreportdiem.SelectedValue.ToString().Trim();
            this.Table_KetQuaTableAdapter.Fill(this.DataSetdiemsv.Table_KetQua,malop,hocki);

            this.reportViewer1.RefreshReport();
        }

        private void cmbhocki_diemsv_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbhocki_diemsv_SelectedValueChanged(object sender, EventArgs e)
        {
       
        }
    }
}
