﻿namespace GUI
{
    partial class FORM_KHOA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FORM_KHOA));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtmakhoa = new System.Windows.Forms.TextBox();
            this.txttenkhoa = new System.Windows.Forms.TextBox();
            this.dtgkhoa = new System.Windows.Forms.DataGridView();
            this.makhoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenkhoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnthemkhoa = new System.Windows.Forms.Button();
            this.btnsuakhoa = new System.Windows.Forms.Button();
            this.btnxoakhoa = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgkhoa)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(317, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã Khoa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(317, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên Khoa";
            // 
            // txtmakhoa
            // 
            this.txtmakhoa.Location = new System.Drawing.Point(387, 39);
            this.txtmakhoa.Name = "txtmakhoa";
            this.txtmakhoa.Size = new System.Drawing.Size(134, 20);
            this.txtmakhoa.TabIndex = 2;
            // 
            // txttenkhoa
            // 
            this.txttenkhoa.Location = new System.Drawing.Point(387, 96);
            this.txttenkhoa.Name = "txttenkhoa";
            this.txttenkhoa.Size = new System.Drawing.Size(134, 20);
            this.txttenkhoa.TabIndex = 3;
            // 
            // dtgkhoa
            // 
            this.dtgkhoa.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgkhoa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgkhoa.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.makhoa,
            this.tenkhoa});
            this.dtgkhoa.Location = new System.Drawing.Point(22, 27);
            this.dtgkhoa.Name = "dtgkhoa";
            this.dtgkhoa.Size = new System.Drawing.Size(277, 184);
            this.dtgkhoa.TabIndex = 4;
            this.dtgkhoa.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgkhoa_CellContentClick);
            // 
            // makhoa
            // 
            this.makhoa.DataPropertyName = "makhoa";
            this.makhoa.HeaderText = "Mã Khoa";
            this.makhoa.Name = "makhoa";
            // 
            // tenkhoa
            // 
            this.tenkhoa.DataPropertyName = "tenkhoa";
            this.tenkhoa.HeaderText = "Tên Khoa";
            this.tenkhoa.Name = "tenkhoa";
            // 
            // btnthemkhoa
            // 
            this.btnthemkhoa.ForeColor = System.Drawing.Color.Blue;
            this.btnthemkhoa.Location = new System.Drawing.Point(67, 231);
            this.btnthemkhoa.Name = "btnthemkhoa";
            this.btnthemkhoa.Size = new System.Drawing.Size(75, 23);
            this.btnthemkhoa.TabIndex = 5;
            this.btnthemkhoa.Text = "Thêm";
            this.btnthemkhoa.UseVisualStyleBackColor = true;
            this.btnthemkhoa.Click += new System.EventHandler(this.btnthemkhoa_Click);
            // 
            // btnsuakhoa
            // 
            this.btnsuakhoa.ForeColor = System.Drawing.Color.Blue;
            this.btnsuakhoa.Location = new System.Drawing.Point(187, 231);
            this.btnsuakhoa.Name = "btnsuakhoa";
            this.btnsuakhoa.Size = new System.Drawing.Size(75, 23);
            this.btnsuakhoa.TabIndex = 6;
            this.btnsuakhoa.Text = "Sửa";
            this.btnsuakhoa.UseVisualStyleBackColor = true;
            this.btnsuakhoa.Click += new System.EventHandler(this.btnsuakhoa_Click);
            // 
            // btnxoakhoa
            // 
            this.btnxoakhoa.ForeColor = System.Drawing.Color.Blue;
            this.btnxoakhoa.Location = new System.Drawing.Point(296, 231);
            this.btnxoakhoa.Name = "btnxoakhoa";
            this.btnxoakhoa.Size = new System.Drawing.Size(75, 23);
            this.btnxoakhoa.TabIndex = 7;
            this.btnxoakhoa.Text = "Xoá";
            this.btnxoakhoa.UseVisualStyleBackColor = true;
            this.btnxoakhoa.Click += new System.EventHandler(this.btnxoakhoa_Click);
            // 
            // button4
            // 
            this.button4.ForeColor = System.Drawing.Color.Blue;
            this.button4.Location = new System.Drawing.Point(413, 231);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 8;
            this.button4.Text = "Thoát";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // FORM_KHOA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gold;
            this.ClientSize = new System.Drawing.Size(564, 277);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnxoakhoa);
            this.Controls.Add(this.btnsuakhoa);
            this.Controls.Add(this.btnthemkhoa);
            this.Controls.Add(this.dtgkhoa);
            this.Controls.Add(this.txttenkhoa);
            this.Controls.Add(this.txtmakhoa);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FORM_KHOA";
            this.Text = "Khoa";
            this.Load += new System.EventHandler(this.FORM_KHOA_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgkhoa)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtmakhoa;
        private System.Windows.Forms.TextBox txttenkhoa;
        private System.Windows.Forms.DataGridView dtgkhoa;
        private System.Windows.Forms.Button btnthemkhoa;
        private System.Windows.Forms.Button btnsuakhoa;
        private System.Windows.Forms.Button btnxoakhoa;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridViewTextBoxColumn makhoa;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenkhoa;
    }
}