﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace DAL
{
    public class DAL_MONHOC
    {
       static string constring = ConfigurationManager.ConnectionStrings["qldiemsv"].ToString();
       public static SqlConnection con = new SqlConnection(constring);
        public static DataTable loadmamonhoc()
        {
            SqlCommand cmd = new SqlCommand("select * from table_monhoc", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable loadsotinchi(DTO_MONHOC dtmh)
        {
            SqlCommand cmd = new SqlCommand("select SOTC from table_monhoc where mamon = @mamon", con);
            cmd.Parameters.AddWithValue("@mamon", dtmh.Mamon);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static void themmonhoc(DTO_MONHOC mh)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into table_monhoc values(@mamon,@tenmon,@sotc,@hocki,@makhoa)", con);
            cmd.Parameters.AddWithValue("@mamon", mh.Mamon);
            cmd.Parameters.AddWithValue("@tenmon", mh.Tenmon);
            cmd.Parameters.AddWithValue("@sotc", mh.Sotc);
            cmd.Parameters.AddWithValue("@hocki", mh.Hocki);
            cmd.Parameters.AddWithValue("@makhoa", mh.Makhoa);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
            con.Close();
           
           
        }
        public static void xoamonhoc(DTO_MONHOC mh)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("delete from table_monhoc where mamon = @mamon and hocki = @hocki", con);
            cmd.Parameters.AddWithValue("@mamon", mh.Mamon);
            cmd.Parameters.AddWithValue("@tenmon", mh.Tenmon);
            cmd.Parameters.AddWithValue("@sotc", mh.Sotc);
            cmd.Parameters.AddWithValue("@hocki", mh.Hocki);
            cmd.Parameters.AddWithValue("@makhoa", mh.Makhoa);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void suamonhoc(DTO_MONHOC mh)
        {
           
                con.Open();
                SqlCommand cmd = new SqlCommand("update table_monhoc set sotc =@sotc,tenmon = @tenmon,makhoa =@makhoa where mamon = @mamon and hocki =@hocki  ", con);
                cmd.Parameters.AddWithValue("@mamon", mh.Mamon);
                cmd.Parameters.AddWithValue("@tenmon", mh.Tenmon);
                cmd.Parameters.AddWithValue("@sotc", mh.Sotc);
                cmd.Parameters.AddWithValue("@hocki", mh.Hocki);
                cmd.Parameters.AddWithValue("@makhoa", mh.Makhoa);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.ExecuteNonQuery();
                con.Close();
           

        }
        public static void xoadltrung()
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("xoadltrung", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
            con.Close();
        }

    }
}
