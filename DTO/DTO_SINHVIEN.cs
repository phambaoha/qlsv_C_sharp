﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class DTO_SINHVIEN
    {
        string _masv;

        public string Masv
        {
            get { return _masv; }
            set { _masv = value; }
        }
        string _hoten;

        public string Hoten
        {
            get { return _hoten; }
            set { _hoten = value; }
        }
        DateTime _ngaysinh;

        public DateTime Ngaysinh
        {
            get { return _ngaysinh; }
            set { _ngaysinh = value; }
        } 
       string _giotinh;

        public string Giotinh
        {
            get { return _giotinh; }
            set { _giotinh = value; }
        }

        string _diachi;

        public string Diachi
        {
            get { return _diachi; }
            set { _diachi = value; }
        }
        string _malop;

        public string Malop
        {
            get { return _malop; }
            set { _malop = value; }
        }
        byte[] _hinhanh;

        public byte[] Hinhanh
        {
            get { return _hinhanh; }
            set { _hinhanh = value; }
        }
      
        public DTO_SINHVIEN(string tmasv, string thoten, DateTime tngaysinh, string tgioitinh, string tdiachi,string tmalop, byte[] tanh)
        {
            _masv = tmasv;
            _hoten = thoten;
            _ngaysinh = tngaysinh;
            _diachi = tdiachi;
            _giotinh = tgioitinh;
            _malop = tmalop;
            _hinhanh = tanh;
       
            
        }
       public DTO_SINHVIEN(string tmasv)
       {
           _masv = tmasv;
       }

       public DTO_SINHVIEN(string tmasv, string thoten)
       {
           _masv = tmasv;
           _hoten = thoten;
       }
       public DTO_SINHVIEN(string tmasv, string tmalop, string thoten)
       {
           _masv = tmasv;
           _malop = tmalop;
           _hoten = thoten;        
       }
     
    }
}
