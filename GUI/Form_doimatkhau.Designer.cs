﻿namespace GUI
{
    partial class Form_doimatkhau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_doimatkhau));
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtmkcu = new System.Windows.Forms.TextBox();
            this.txtmkmoi = new System.Windows.Forms.TextBox();
            this.txtnhaplaimkmoi = new System.Windows.Forms.TextBox();
            this.btnsuathongtin = new System.Windows.Forms.Button();
            this.txttendangnhap = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // txtmkcu
            // 
            resources.ApplyResources(this.txtmkcu, "txtmkcu");
            this.txtmkcu.Name = "txtmkcu";
            this.txtmkcu.TextChanged += new System.EventHandler(this.txtmkcu_TextChanged);
            // 
            // txtmkmoi
            // 
            resources.ApplyResources(this.txtmkmoi, "txtmkmoi");
            this.txtmkmoi.Name = "txtmkmoi";
            // 
            // txtnhaplaimkmoi
            // 
            resources.ApplyResources(this.txtnhaplaimkmoi, "txtnhaplaimkmoi");
            this.txtnhaplaimkmoi.Name = "txtnhaplaimkmoi";
            this.txtnhaplaimkmoi.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // btnsuathongtin
            // 
            this.btnsuathongtin.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.btnsuathongtin, "btnsuathongtin");
            this.btnsuathongtin.Name = "btnsuathongtin";
            this.btnsuathongtin.UseVisualStyleBackColor = true;
            this.btnsuathongtin.Click += new System.EventHandler(this.btnsuathongtin_Click);
            // 
            // txttendangnhap
            // 
            resources.ApplyResources(this.txttendangnhap, "txttendangnhap");
            this.txttendangnhap.Name = "txttendangnhap";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form_doimatkhau
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txttendangnhap);
            this.Controls.Add(this.btnsuathongtin);
            this.Controls.Add(this.txtnhaplaimkmoi);
            this.Controls.Add(this.txtmkmoi);
            this.Controls.Add(this.txtmkcu);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.MaximizeBox = false;
            this.Name = "Form_doimatkhau";
            this.Load += new System.EventHandler(this.Form_doimatkhau_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtmkcu;
        private System.Windows.Forms.TextBox txtmkmoi;
        private System.Windows.Forms.TextBox txtnhaplaimkmoi;
        private System.Windows.Forms.Button btnsuathongtin;
        private System.Windows.Forms.TextBox txttendangnhap;
        private System.Windows.Forms.Button button1;
    }
}