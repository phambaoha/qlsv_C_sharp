﻿namespace GUI
{
    partial class Form_MONHOC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtgmonhoc = new System.Windows.Forms.DataGridView();
            this.mamon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenmon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sotc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hocki = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.makhoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtmakhoa = new System.Windows.Forms.TextBox();
            this.txtsotinchi = new System.Windows.Forms.TextBox();
            this.txttenmon = new System.Windows.Forms.TextBox();
            this.btnthemmonhoc = new System.Windows.Forms.Button();
            this.btnsuamonhoc = new System.Windows.Forms.Button();
            this.btnxoamonhoc = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnxoatrung = new System.Windows.Forms.Button();
            this.cmbmamon = new System.Windows.Forms.ComboBox();
            this.cmbhocki = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtgmonhoc)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgmonhoc
            // 
            this.dtgmonhoc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgmonhoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgmonhoc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mamon,
            this.tenmon,
            this.sotc,
            this.hocki,
            this.makhoa});
            this.dtgmonhoc.Location = new System.Drawing.Point(20, 48);
            this.dtgmonhoc.Name = "dtgmonhoc";
            this.dtgmonhoc.Size = new System.Drawing.Size(420, 227);
            this.dtgmonhoc.TabIndex = 0;
            this.dtgmonhoc.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgmonhoc_CellContentClick);
            // 
            // mamon
            // 
            this.mamon.DataPropertyName = "mamon";
            this.mamon.HeaderText = "Mã Môn";
            this.mamon.Name = "mamon";
            // 
            // tenmon
            // 
            this.tenmon.DataPropertyName = "tenmon";
            this.tenmon.HeaderText = "Tên Môn";
            this.tenmon.Name = "tenmon";
            // 
            // sotc
            // 
            this.sotc.DataPropertyName = "sotc";
            this.sotc.HeaderText = "Số TC";
            this.sotc.Name = "sotc";
            // 
            // hocki
            // 
            this.hocki.DataPropertyName = "hocki";
            this.hocki.HeaderText = "Học kì";
            this.hocki.Name = "hocki";
            // 
            // makhoa
            // 
            this.makhoa.DataPropertyName = "makhoa";
            this.makhoa.HeaderText = "Mã Khoa";
            this.makhoa.Name = "makhoa";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(466, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã môn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(462, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên môn";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(462, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Số tín chỉ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(466, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Học kì";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(462, 212);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Mã khoa";
            // 
            // txtmakhoa
            // 
            this.txtmakhoa.Location = new System.Drawing.Point(537, 209);
            this.txtmakhoa.Name = "txtmakhoa";
            this.txtmakhoa.Size = new System.Drawing.Size(169, 20);
            this.txtmakhoa.TabIndex = 7;
            // 
            // txtsotinchi
            // 
            this.txtsotinchi.Location = new System.Drawing.Point(537, 163);
            this.txtsotinchi.Name = "txtsotinchi";
            this.txtsotinchi.Size = new System.Drawing.Size(169, 20);
            this.txtsotinchi.TabIndex = 9;
            // 
            // txttenmon
            // 
            this.txttenmon.Location = new System.Drawing.Point(537, 122);
            this.txttenmon.Name = "txttenmon";
            this.txttenmon.Size = new System.Drawing.Size(169, 20);
            this.txttenmon.TabIndex = 10;
            // 
            // btnthemmonhoc
            // 
            this.btnthemmonhoc.ForeColor = System.Drawing.Color.Blue;
            this.btnthemmonhoc.Location = new System.Drawing.Point(45, 301);
            this.btnthemmonhoc.Name = "btnthemmonhoc";
            this.btnthemmonhoc.Size = new System.Drawing.Size(111, 27);
            this.btnthemmonhoc.TabIndex = 11;
            this.btnthemmonhoc.Text = "Thêm";
            this.btnthemmonhoc.UseVisualStyleBackColor = true;
            this.btnthemmonhoc.Click += new System.EventHandler(this.btnthemmonhoc_Click);
            // 
            // btnsuamonhoc
            // 
            this.btnsuamonhoc.ForeColor = System.Drawing.Color.Blue;
            this.btnsuamonhoc.Location = new System.Drawing.Point(182, 301);
            this.btnsuamonhoc.Name = "btnsuamonhoc";
            this.btnsuamonhoc.Size = new System.Drawing.Size(111, 27);
            this.btnsuamonhoc.TabIndex = 12;
            this.btnsuamonhoc.Text = "Sửa";
            this.btnsuamonhoc.UseVisualStyleBackColor = true;
            this.btnsuamonhoc.Click += new System.EventHandler(this.btnsuamonhoc_Click);
            // 
            // btnxoamonhoc
            // 
            this.btnxoamonhoc.ForeColor = System.Drawing.Color.Blue;
            this.btnxoamonhoc.Location = new System.Drawing.Point(315, 301);
            this.btnxoamonhoc.Name = "btnxoamonhoc";
            this.btnxoamonhoc.Size = new System.Drawing.Size(111, 27);
            this.btnxoamonhoc.TabIndex = 13;
            this.btnxoamonhoc.Text = "Xoá";
            this.btnxoamonhoc.UseVisualStyleBackColor = true;
            this.btnxoamonhoc.Click += new System.EventHandler(this.btnxoamonhoc_Click);
            // 
            // button4
            // 
            this.button4.ForeColor = System.Drawing.Color.Blue;
            this.button4.Location = new System.Drawing.Point(565, 301);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(111, 27);
            this.button4.TabIndex = 14;
            this.button4.Text = "Thoát";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnxoatrung
            // 
            this.btnxoatrung.ForeColor = System.Drawing.Color.Blue;
            this.btnxoatrung.Location = new System.Drawing.Point(436, 301);
            this.btnxoatrung.Name = "btnxoatrung";
            this.btnxoatrung.Size = new System.Drawing.Size(111, 27);
            this.btnxoatrung.TabIndex = 15;
            this.btnxoatrung.Text = "Xoá DL Trùng";
            this.btnxoatrung.UseVisualStyleBackColor = true;
            this.btnxoatrung.Click += new System.EventHandler(this.btnxoatrung_Click);
            // 
            // cmbmamon
            // 
            this.cmbmamon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbmamon.FormattingEnabled = true;
            this.cmbmamon.Location = new System.Drawing.Point(537, 80);
            this.cmbmamon.Name = "cmbmamon";
            this.cmbmamon.Size = new System.Drawing.Size(169, 21);
            this.cmbmamon.TabIndex = 16;
            this.cmbmamon.SelectedValueChanged += new System.EventHandler(this.cmbmamon_SelectedValueChanged);
            // 
            // cmbhocki
            // 
            this.cmbhocki.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbhocki.FormattingEnabled = true;
            this.cmbhocki.Location = new System.Drawing.Point(537, 39);
            this.cmbhocki.Name = "cmbhocki";
            this.cmbhocki.Size = new System.Drawing.Size(169, 21);
            this.cmbhocki.TabIndex = 17;
            this.cmbhocki.SelectionChangeCommitted += new System.EventHandler(this.cmbhocki_SelectionChangeCommitted);
            this.cmbhocki.SelectedValueChanged += new System.EventHandler(this.cmbhocki_SelectedValueChanged);
            this.cmbhocki.SizeChanged += new System.EventHandler(this.cmbhocki_SizeChanged);
            // 
            // Form_MONHOC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gold;
            this.ClientSize = new System.Drawing.Size(746, 363);
            this.Controls.Add(this.cmbhocki);
            this.Controls.Add(this.cmbmamon);
            this.Controls.Add(this.btnxoatrung);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnxoamonhoc);
            this.Controls.Add(this.btnsuamonhoc);
            this.Controls.Add(this.btnthemmonhoc);
            this.Controls.Add(this.txttenmon);
            this.Controls.Add(this.txtsotinchi);
            this.Controls.Add(this.txtmakhoa);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtgmonhoc);
            this.Name = "Form_MONHOC";
            this.Text = "Môn Học";
            this.Load += new System.EventHandler(this.Form_MONHOC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgmonhoc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgmonhoc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtmakhoa;
        private System.Windows.Forms.TextBox txtsotinchi;
        private System.Windows.Forms.TextBox txttenmon;
        private System.Windows.Forms.Button btnthemmonhoc;
        private System.Windows.Forms.Button btnsuamonhoc;
        private System.Windows.Forms.Button btnxoamonhoc;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnxoatrung;
        private System.Windows.Forms.DataGridViewTextBoxColumn mamon;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenmon;
        private System.Windows.Forms.DataGridViewTextBoxColumn sotc;
        private System.Windows.Forms.DataGridViewTextBoxColumn hocki;
        private System.Windows.Forms.DataGridViewTextBoxColumn makhoa;
        private System.Windows.Forms.ComboBox cmbmamon;
        private System.Windows.Forms.ComboBox cmbhocki;
    }
}