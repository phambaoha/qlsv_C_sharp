﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_LOGIN
    {
        string _madn;

        public string Madn
        {
            get { return _madn; }
            set { _madn = value; }
        }
        string _matkhau;

        public string Matkhau
        {
            get { return _matkhau; }
            set { _matkhau = value; }
        }
        public DTO_LOGIN(string tmadn, string tmatkhau)
        {
            _madn = tmadn;
            _matkhau = tmatkhau;
        }
         
        public DTO_LOGIN(string tmadn)
        {
            _madn = tmadn;
        }
        string _quyen;

        public string Quyen
        {
            get { return _quyen; }
            set { _quyen = value; }
        }
       public DTO_LOGIN(string tmadn,string tmatkhau,string tquyen)
        {
            _madn = tmadn;
            _matkhau = tmatkhau;
            _quyen = tquyen;
        }
     
    }
}
