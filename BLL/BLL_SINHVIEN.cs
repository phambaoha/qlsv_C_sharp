﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;
using System.Data;

namespace BLL
{
    public class BLL_SINHVIEN
    {
        public static DataTable laydulieu()
        {
            return DAL_SINHVIEN.getdata();
        }
        public static void them(DTO_SINHVIEN sv)
        {
            DAL_SINHVIEN.them(sv);
        }
        public static void xoa(DTO_SINHVIEN sv)
        {
            DAL_SINHVIEN.xoa(sv);
        }
        public static void sua(DTO_SINHVIEN sv)
        {
            DAL_SINHVIEN.sua(sv);
        }
         public static DataTable loaddlcomboboxlop()
        {
            return DAL_SINHVIEN.loaddlcomboboxlop();
        }
         public static DataTable loaddulieucomboboxhoten(DTO_LOP dtlop)
         {
             return DAL_SINHVIEN.loaddulieucomboboxhoten(dtlop);
         }
       public static DataTable loadddtxthoten(DTO_SINHVIEN sv)
         {
             return DAL_SINHVIEN.loadddtxthoten(sv);
         }
        public static DataTable timkiemsv(DTO_SINHVIEN sv)
       {
           return DAL_SINHVIEN.timkiemsv(sv);
       }
        public static DataTable timkiemmalop(DTO_LOP lp)
        {
            return DAL_SINHVIEN.timkiemmalop(lp);
        }
         
    }
}
