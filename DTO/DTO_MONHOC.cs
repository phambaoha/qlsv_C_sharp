﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
  public class DTO_MONHOC
    {
        string _mamon;

        public string Mamon
        {
            get { return _mamon; }
            set { _mamon = value; }
        }
        string _tenmon;

        public string Tenmon
        {
            get { return _tenmon; }
            set { _tenmon = value; }
        }
        private int _sotc;

        public int Sotc
        {
            get { return _sotc; }
            set { _sotc = value; }
        }
        string _hocki;

        public string Hocki
        {
            get { return _hocki; }
            set { _hocki = value; }
        }
       string _makhoa;

    public string Makhoa
    {
    get { return _makhoa; }
    set { _makhoa = value; }
    }
      public DTO_MONHOC(string tmamon)
        {
            _mamon = tmamon;
        }
    
      public DTO_MONHOC(string tmamon, int ttinchi)
      {
          _mamon = tmamon;
          _sotc = ttinchi;
      }
      public DTO_MONHOC(string tmamon, string ttenmon, int tsotinchi, string thocki, string tmakhoa)
      {
          _mamon = tmamon;
          _tenmon = ttenmon;
          _sotc = tsotinchi;
          _hocki = thocki;
          _makhoa = tmakhoa;
      }

    }
}
