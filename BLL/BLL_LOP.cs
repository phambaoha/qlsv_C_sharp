﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;
using System.Data;

namespace BLL
{
    public class BLL_LOP
    {
        public static DataTable loaddlcomboboxlop1()
        {
            return DAL_LOP.loaddlcomboboxlop1();
        }
         public static DataTable loaddlcomboxmalop()
        {
            return DAL_LOP.loaddlcomboxmalop();
        }
         public static void themlop(DTO_LOP lp)
         {
             DAL_LOP.themlop(lp);
         }
         public static void sualop(DTO_LOP lp)
         {
             DAL_LOP.sualop(lp);
         }
         public static void xoalop(DTO_LOP lp)
         {
             DAL_LOP.xoalop(lp);
         }
         public static DataTable xemtoanfullhockitheomalop_thongke(DTO_LOP lp)
         {
             return DAL_LOP.xemtoanfullhockitheomalop_thongke(lp);
         }
         public static DataTable loc_diem_gioi(DTO_LOP lp)
         {
             return DAL_LOP.loc_diem_gioi(lp);
         }
          public static DataTable loc_diem_kha(DTO_LOP lp)
         {
             return DAL_LOP.loc_diem_kha(lp);
         }
         public static DataTable loc_diem_tb(DTO_LOP lp)
          {
              return DAL_LOP.loc_diem_tb(lp);
          }
          public static DataTable loc_diem_yeu(DTO_LOP lp)
         {
             return DAL_LOP.loc_diem_yeu(lp);
         }
          public static DataTable timkiemhocbong(DTO_LOP lp)
          {
              return DAL_LOP.timkiemhocbong(lp);
          }
    }
}
