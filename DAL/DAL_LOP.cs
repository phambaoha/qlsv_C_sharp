﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace DAL
{
    public class DAL_LOP
    {


        static string constring = ConfigurationManager.ConnectionStrings["qldiemsv"].ToString();
         public static SqlConnection con = new SqlConnection(constring);
        public static DataTable loaddlcomboboxlop1()
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from table_lop", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable loaddlcomboxmalop()
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from table_lop", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static void themlop(DTO_LOP lp)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into table_lop values(@malop,@tenlop,@makhoa)", con);
            cmd.Parameters.AddWithValue("@malop", lp.Malop);
            cmd.Parameters.AddWithValue("@tenlop", lp.Tenlop);
            cmd.Parameters.AddWithValue("@makhoa", lp.Makhoa);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void sualop(DTO_LOP lp)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(" update table_lop set tenlop = @tenlop,makhoa = @makhoa where malop = @malop ", con);
            cmd.Parameters.AddWithValue("@malop", lp.Malop);
            cmd.Parameters.AddWithValue("@tenlop", lp.Tenlop);
            cmd.Parameters.AddWithValue("@makhoa", lp.Makhoa);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public static void xoalop(DTO_LOP lp)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(" delete from table_lop where malop = @malop ", con);
            cmd.Parameters.AddWithValue("@malop", lp.Malop);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static DataTable xemtoanfullhockitheomalop_thongke(DTO_LOP lp)
        {
            SqlCommand cmd = new SqlCommand("select kq.masv as N'Mã SV',kq.hoten as N'Họ Tên', cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) as N'Điểm TB Chung Tích Luỹ',cast( (avg(kq.diemmonhoc)) as decimal(18,2)) as N'Điểm Toàn Khoá Hệ 10' from table_ketqua kq inner join table_lop lp on kq.MALOP = lp.malop where kq.Malop = @malop  group by kq.masv,kq.hoten", con);
            cmd.Parameters.AddWithValue("@malop", lp.Malop);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable loc_diem_gioi(DTO_LOP lp)
        {
            SqlCommand cmd = new SqlCommand("select kq.masv as N'Mã SV',kq.hoten as N'Họ Tên', cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) as N'Điểm TB Chung Tích Luỹ',cast( (avg(kq.diemmonhoc)) as decimal(18,2)) as N'Điểm Toàn Khoá Hệ 10' from table_ketqua kq inner join table_lop lp on kq.MALOP = lp.malop where kq.Malop = @malop  group by kq.masv,kq.hoten having  cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) >= 3.2 ", con);
            cmd.Parameters.AddWithValue("@malop", lp.Malop);      
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable loc_diem_kha(DTO_LOP lp)
        {
            SqlCommand cmd = new SqlCommand("select kq.masv as N'Mã SV',kq.hoten as N'Họ Tên', cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) as N'Điểm TB Chung Tích Luỹ',cast( (avg(kq.diemmonhoc)) as decimal(18,2)) as N'Điểm Toàn Khoá Hệ 10' from table_ketqua kq inner join table_lop lp on kq.MALOP = lp.malop where kq.Malop = @malop  group by kq.masv,kq.hoten having  cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) >=2.5 and cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2))<3.2 ", con);
            cmd.Parameters.AddWithValue("@malop", lp.Malop);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable loc_diem_tb(DTO_LOP lp)
        {
            SqlCommand cmd = new SqlCommand("select kq.masv as N'Mã SV',kq.hoten as N'Họ Tên', cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) as N'Điểm TB Chung Tích Luỹ',cast( (avg(kq.diemmonhoc)) as decimal(18,2)) as N'Điểm Toàn Khoá Hệ 10' from table_ketqua kq inner join table_lop lp on kq.MALOP = lp.malop where kq.Malop = @malop  group by kq.masv,kq.hoten having  cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) >= 2.0 and cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) <2.5  ", con);
            cmd.Parameters.AddWithValue("@malop", lp.Malop);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable loc_diem_yeu(DTO_LOP lp)
        {
            SqlCommand cmd = new SqlCommand("select kq.masv as N'Mã SV',kq.hoten as N'Họ Tên', cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) as N'Điểm TB Chung Tích Luỹ',cast( (avg(kq.diemmonhoc)) as decimal(18,2)) as N'Điểm Toàn Khoá Hệ 10' from table_ketqua kq inner join table_lop lp on kq.MALOP = lp.malop where kq.Malop = @malop  group by kq.masv,kq.hoten having  cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) < 2.0 ", con);
            cmd.Parameters.AddWithValue("@malop", lp.Malop);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable timkiemhocbong(DTO_LOP lp)
        {
            SqlCommand cmd = new SqlCommand("select kq.masv as N'Mã SV',kq.hoten as N'Họ Tên', cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) as N'Điểm TB Chung Tích Luỹ',cast( (avg(kq.diemmonhoc)) as decimal(18,2)) as N'Điểm Toàn Khoá Hệ 10', kq.diemrenluyen as N'Điểm Rèn Luyện' from table_ketqua kq inner join table_lop lp on kq.MALOP = lp.malop where kq.Malop = @malop and kq.diemrenluyen >=80  group by kq.masv,kq.hoten having  cast( (sum(kq.diemtonghebon)/sum(sotc)) as decimal(18,2)) < 2.0", con);
            cmd.Parameters.AddWithValue("@malop", lp.Malop);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
    }
}
