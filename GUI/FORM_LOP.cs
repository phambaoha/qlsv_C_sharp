﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;
using DAL;
namespace GUI
{
    public partial class FORM_LOP : Form
    {
        public FORM_LOP()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FORM_LOP_Load(object sender, EventArgs e)
        {
            dtglop.DataSource = BLL_LOP.loaddlcomboxmalop();
        }

        private void dtglop_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dtglop.CurrentRow.Index;
            txtmalop.Text = dtglop[0, index].Value.ToString();
            txttenlop.Text = dtglop[1, index].Value.ToString();
            txtmakhoa.Text = dtglop[2, index].Value.ToString();
        }

        private void btnthemlop_Click(object sender, EventArgs e)
        {
            try
            {
                DTO_LOP dtlop = new DTO_LOP(txtmalop.Text.ToUpper(), txttenlop.Text, txtmakhoa.Text);
                BLL_LOP.themlop(dtlop);
                dtglop.DataSource = BLL_LOP.loaddlcomboxmalop();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnsualop_Click(object sender, EventArgs e)
        {
            try
            {
                DTO_LOP dtlop = new DTO_LOP(txtmalop.Text.ToUpper(), txttenlop.Text, txtmakhoa.Text);
                BLL_LOP.sualop(dtlop);
                dtglop.DataSource = BLL_LOP.loaddlcomboxmalop();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                DAL.DAL_LOP.con.Close();
            }

        }

        private void btnxoalop_Click(object sender, EventArgs e)
        {
            try
            {
                DTO_LOP dtlop = new DTO_LOP(txtmalop.Text);
                BLL_LOP.xoalop(dtlop);
                dtglop.DataSource = BLL_LOP.loaddlcomboxmalop();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
