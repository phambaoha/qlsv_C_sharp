﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DAL
{
  public class DAL_KETQUA
    {
      static string constring = ConfigurationManager.ConnectionStrings["qldiemsv"].ConnectionString.ToString();
      static  SqlConnection con = new SqlConnection(constring);
      public static DataTable loaddlcomboboxhocki()
      {
          SqlCommand cmd = new SqlCommand("select * from laydl_tblketqua", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
      }
      //insert into table_ketqua values(@masv, @hoten, @hocki, @mamon, @malop,@diema, @diemb,@diemc, @diemmonhoc, @diemchu, @diemrenluyen)
      public static void themdiemsv(DTO_KETQUA kq)
      {

          SqlCommand cmd = new SqlCommand("them_tblketqua", con);
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@masv", kq.Masv);
          cmd.Parameters.AddWithValue("@hoten", kq.Hoten);
          cmd.Parameters.AddWithValue("@hocki", kq.Hocki);
          cmd.Parameters.AddWithValue("@mamon", kq.Mamon);
          cmd.Parameters.AddWithValue("@malop", kq.Malop);
          cmd.Parameters.AddWithValue("@diema", kq.Diema);
          cmd.Parameters.AddWithValue("@diemb", kq.Diemb);
          cmd.Parameters.AddWithValue("@diemc", kq.Diemc);
          cmd.Parameters.AddWithValue("@diemmonhoc", kq.Diemmonhoc);
          cmd.Parameters.AddWithValue("@diemchu", kq.Diemch);
          cmd.Parameters.AddWithValue("@heso", kq.Heso);
          cmd.Parameters.AddWithValue("@diemrenluyen", kq.Diemrenluyen);
          cmd.Parameters.AddWithValue("@sotc", kq.Sotc);
          cmd.Parameters.AddWithValue("@diemtonghebon", kq.Diemtonghebon);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          con.Open();
          cmd.ExecuteNonQuery();
          con.Close();     
      }
     public static void Xoadiemsv(DTO_KETQUA kq)
      {
          SqlCommand cmd = new SqlCommand("xoa_tblketqua ", con);
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@masv",kq.Masv );
          cmd.Parameters.AddWithValue("@mamon",kq.Mamon);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          con.Open();
          cmd.ExecuteNonQuery();
          con.Close(); 
      } 
      public static void suadiemsv(DTO_KETQUA kq)
     {
         SqlCommand cmd = new SqlCommand("sua_tblketqua", con);
         cmd.CommandType = CommandType.StoredProcedure;
         cmd.Parameters.AddWithValue("@masv", kq.Masv);
         cmd.Parameters.AddWithValue("@hoten", kq.Hoten);
         cmd.Parameters.AddWithValue("@hocki", kq.Hocki);
         cmd.Parameters.AddWithValue("@mamon", kq.Mamon);
         cmd.Parameters.AddWithValue("@malop", kq.Malop);
         cmd.Parameters.AddWithValue("@diema", kq.Diema);
         cmd.Parameters.AddWithValue("@diemb", kq.Diemb);
         cmd.Parameters.AddWithValue("@diemc", kq.Diemc);
         cmd.Parameters.AddWithValue("@diemmonhoc",kq.Diemmonhoc);
         cmd.Parameters.AddWithValue("@diemchu", kq.Diemch);
         cmd.Parameters.AddWithValue("@heso", kq.Heso);
         cmd.Parameters.AddWithValue("@diemrenluyen", kq.Diemrenluyen);
         cmd.Parameters.AddWithValue("@sotc", kq.Sotc);
         cmd.Parameters.AddWithValue("@diemtonghebon", kq.Diemtonghebon);
         SqlDataAdapter da = new SqlDataAdapter(cmd);
         con.Open();
         cmd.ExecuteNonQuery();
         con.Close();  
     }
      //select kq.hocki,kq.Masv,kq.hoten,kq.Mamon,kq.Diemmonhoc from (Table_KetQua kq inner join Table_SINHVIEN sv on sv.MASV = kq.Masv on  where sv.masv = @masv
      public static DataTable loaddldiem(DTO_SINHVIEN sv, DTO_HOCKI hk)
      {
          SqlCommand cmd = new SqlCommand("xemdiemtheoki", con);
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@masv", sv.Masv);
          cmd.Parameters.AddWithValue("@hocki",hk.Hocki);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          DataTable dt = new DataTable();
          con.Open();
          da.Fill(dt);
          con.Close();
          return dt;
      }
      public static DataTable xemtoanfullhocki(DTO_SINHVIEN sv)
      {
          SqlCommand cmd = new SqlCommand("xemdiemtichluytoankhoa", con);
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@masv", sv.Masv);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          DataTable dt = new DataTable();
          con.Open();
          da.Fill(dt);
          con.Close();
          return dt;
      }

      public static DataTable diemtong1ki(DTO_SINHVIEN sv,DTO_HOCKI hk)
      {

          SqlCommand cmd = new SqlCommand("xemdiemtichluytheoki", con);
          cmd.CommandType = CommandType.StoredProcedure;
          cmd.Parameters.AddWithValue("@masv", sv.Masv);
          cmd.Parameters.AddWithValue("@hocki",hk.Hocki);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          DataTable dt = new DataTable();
          con.Open();
          da.Fill(dt);
          con.Close();
          return dt;
      }
      public static void updatetable_ketqua()
      {
          con.Open();
          SqlCommand cmd = new SqlCommand("update Table_KetQua set  Malop = Table_SINHVIEN.MALOP, hoten = Table_SINHVIEN.HOTEN from Table_SINHVIEN join Table_KetQua on Table_KetQua.Masv = Table_SINHVIEN.MASV ", con);          
          cmd.ExecuteNonQuery();
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          con.Close();
      }
      public static DataTable timkiemmalop(DTO_LOP lp)
      {
          SqlCommand cmd = new SqlCommand("select * from table_ketqua where malop = @malop", con);
          cmd.Parameters.AddWithValue("@malop", lp.Malop);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          DataTable dt = new DataTable();
          con.Open();
          da.Fill(dt);
          con.Close();
          return dt;
      }
      public static DataTable timkiemhocki(DTO_HOCKI hk)
      {
          SqlCommand cmd = new SqlCommand("select * from table_ketqua where hocki = @hocki", con);
          cmd.Parameters.AddWithValue("@hocki", hk.Hocki);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          DataTable dt = new DataTable();
          con.Open();
          da.Fill(dt);
          con.Close();
          return dt;
      }
      public static DataTable timkiemmamon(DTO_MONHOC mh)
      {
          SqlCommand cmd = new SqlCommand("select * from table_ketqua where mamon = @mamon", con);
          cmd.Parameters.AddWithValue("@mamon", mh.Mamon);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          DataTable dt = new DataTable();
          con.Open();
          da.Fill(dt);
          con.Close();
          return dt;
      }
      public static DataTable timkiemmasv(DTO_SINHVIEN sv)
      {
          SqlCommand cmd = new SqlCommand("select * from table_ketqua where masv = @masv", con);
          cmd.Parameters.AddWithValue("@masv", sv.Masv);
          SqlDataAdapter da = new SqlDataAdapter(cmd);
          DataTable dt = new DataTable();
          con.Open();
          da.Fill(dt);
          con.Close();
          return dt;
      }
   
    }
}
