﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using DTO;
using System.Windows;

namespace DAL
{
    public class DAL_SINHVIEN
    {
        static string constring = ConfigurationManager.ConnectionStrings["qldiemsv"].ToString();
        public static SqlConnection con = new SqlConnection(constring);
        public static DataTable getdata()
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from table_sinhvien", con);
            //cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static void them(DTO_SINHVIEN sv)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("themdlsinhvien", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@masv", sv.Masv);
            cmd.Parameters.AddWithValue("@hoten", sv.Hoten);
            cmd.Parameters.AddWithValue("@ngaysinh", sv.Ngaysinh);
            cmd.Parameters.AddWithValue("@gioitinh", sv.Giotinh);
            cmd.Parameters.AddWithValue("@diachi", sv.Diachi);
            cmd.Parameters.AddWithValue("@malop", sv.Malop);
            cmd.Parameters.AddWithValue("@anh", sv.Hinhanh);
           
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void xoa(DTO_SINHVIEN sv)
        {

            con.Open();
            SqlCommand cmd = new SqlCommand("xoadlsinhvien", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@masv", sv.Masv);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
            con.Close();


        }
        public static void sua(DTO_SINHVIEN sv)
        {

            con.Open();
            SqlCommand cmd = new SqlCommand("suadlsinhvien", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@masv", sv.Masv);
            cmd.Parameters.AddWithValue("@hoten", sv.Hoten);
            cmd.Parameters.AddWithValue("@ngaysinh", sv.Ngaysinh);
            cmd.Parameters.AddWithValue("@gioitinh", sv.Giotinh);
            cmd.Parameters.AddWithValue("@diachi", sv.Diachi);
            cmd.Parameters.AddWithValue("@malop", sv.Malop);
            cmd.Parameters.AddWithValue("@anh", sv.Hinhanh);
           
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static DataTable loaddlcomboboxlop()
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from table_lop ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        //select * from table_sinhvien sv inner join table_lop lp on sv.malop = lp.malop where lp.malop = @malop
        public static DataTable loaddulieucomboboxhoten(DTO_LOP dtlop)
        {
            SqlCommand cmd = new SqlCommand("select * from table_sinhvien sv inner join table_lop lp on sv.malop = lp.malop where lp.malop = @malop", con);
            cmd.Parameters.AddWithValue("@malop", dtlop.Malop);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable loadddtxthoten(DTO_SINHVIEN sv)
        {
            SqlCommand cmd = new SqlCommand("select hoten from table_sinhvien where masv = @masv", con);
            cmd.Parameters.AddWithValue("@masv", sv.Masv);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable timkiemsv(DTO_SINHVIEN sv)
        {
            SqlCommand cmd = new SqlCommand("select * from table_sinhvien where masv = @masv", con);
            cmd.Parameters.AddWithValue("@masv", sv.Masv);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable timkiemmalop(DTO_LOP lp)
        {
            SqlCommand cmd = new SqlCommand("select * from table_sinhvien where malop = @malop", con);
            cmd.Parameters.AddWithValue("@malop", lp.Malop);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
    }
}
