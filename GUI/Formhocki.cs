﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;
namespace GUI
{
    public partial class Formhocki : Form
    {
        public Formhocki()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Formhocki_Load(object sender, EventArgs e)
        {
            dtghocki.DataSource = BLL_HOCKI.loadhocki();
        }

        private void dtghocki_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dtghocki.CurrentRow.Index;
            txthocki.Text = dtghocki[0, index].Value.ToString();
            txtmamon.Text = dtghocki[1, index].Value.ToString();
        }

        private void btnthemlop_Click(object sender, EventArgs e)
        {
            DTO_HOCKI dthk = new DTO_HOCKI(txthocki.Text, txtmamon.Text);
            BLL_HOCKI.themhocki(dthk);
            dtghocki.DataSource = BLL_HOCKI.loadhocki();
        }

        private void btnsualop_Click(object sender, EventArgs e)
        {
            DTO_HOCKI dthk = new DTO_HOCKI(txthocki.Text, txtmamon.Text);
            BLL_HOCKI.suahocki(dthk);
            dtghocki.DataSource = BLL_HOCKI.loadhocki();
        }

        private void btnxoahocki_Click(object sender, EventArgs e)
        {
            DTO_HOCKI dthk = new DTO_HOCKI(txthocki.Text, txtmamon.Text);
            BLL_HOCKI.xoahocki(dthk);
            dtghocki.DataSource = BLL_HOCKI.loadhocki();
        }
    }
}
