﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;
using System.Security.Cryptography;
namespace GUI
{ 
    
    public partial class Form_doimatkhau : Form
    {
        public Form_doimatkhau()
        {
            InitializeComponent();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        public string GetMD5(string chuoi)
        {
            string str_md5 = "";
            byte[] mang = System.Text.Encoding.UTF8.GetBytes(chuoi);

            MD5CryptoServiceProvider my_md5 = new MD5CryptoServiceProvider();
            mang = my_md5.ComputeHash(mang);

            foreach (byte b in mang)
            {
                str_md5 += b.ToString("X2");
            }

            return str_md5;
        }
        private void btnsuathongtin_Click(object sender, EventArgs e)
        {
            
            if (txtmkcu.Text != Formlogin.a)
                MessageBox.Show(" Mật khẩu cũ chưa có hoặc không chính xác");
            else if (txtmkmoi.Text == "")
                MessageBox.Show("Chưa nhập mật khẩu mới!");
            else if (txtnhaplaimkmoi.Text == "")
                MessageBox.Show("Chưa nhập lại mật khẩu mới!");
                else if(txtmkmoi.Text.ToString().Trim() != txtnhaplaimkmoi.Text.ToString().Trim())
                MessageBox.Show("Mật khẩu nhập lại chưa khớp!");
            else
            {
                
                DTO_LOGIN dt = new DTO_LOGIN(txttendangnhap.Text, GetMD5(txtnhaplaimkmoi.Text));
                BLL_LOGIN.suamatkhau(dt);
                
                DialogResult traloi;
                traloi = MessageBox.Show("Bạn chắc chắn muốn lưu", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (traloi == DialogResult.Yes)
                    MessageBox.Show("Lưu thành công, Đăng xuất và nhập lại Mật Khẩu mới! ");

                this.Close();
            }
        }

        private void Form_doimatkhau_Load(object sender, EventArgs e)
        {
            txttendangnhap.Text = Formlogin.b;
            txtmkmoi.UseSystemPasswordChar = true;
            txtnhaplaimkmoi.UseSystemPasswordChar = true;
            
        }

        private void txtmkcu_TextChanged(object sender, EventArgs e)
        {
            txtmkcu.UseSystemPasswordChar = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }      
    }
}
