﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;
using System.Data;
namespace BLL
{
    public class BLL_MONHOC
    {
         public static DataTable loadmamonhoc()
        {
            return DAL_MONHOC.loadmamonhoc();
        }
           public static DataTable loadsotinchi(DTO_MONHOC dtmh)
         {
             return DAL_MONHOC.loadsotinchi(dtmh);
         }
         public static void themmonhoc(DTO_MONHOC mh)
           {
               DAL_MONHOC.themmonhoc(mh);
           }
        public static void xoamonhoc(DTO_MONHOC mh)
         {
             DAL_MONHOC.xoamonhoc(mh);
         }
        public static void suamonhoc(DTO_MONHOC mh)
        {
            DAL_MONHOC.suamonhoc(mh);
        }
        public static void xoadltrung()
        {
            DAL_MONHOC.xoadltrung();
        }
    }
}
