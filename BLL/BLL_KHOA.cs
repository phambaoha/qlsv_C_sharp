﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;
using System.Data;
namespace BLL
{
    public class BLL_KHOA
    {
         public static DataTable laydlmakhoa()
        {
            return DAL_KHOA.laydlmakhoa();
        }
        public static DataTable loadcomboboxmalop(DTO_KHOA kh)
         {
             return DAL_KHOA.loadcomboboxmalop(kh);
         }
         public static void themkhoa(DTO_KHOA kh)
        {
               DAL_KHOA.themkhoa(kh);
        }
         public static void suakhoa(DTO_KHOA kh)
         {
             DAL_KHOA.suakhoa(kh);
         }
        public static void xoakhoa(DTO_KHOA kh)
         {
             DAL_KHOA.xoakhoa(kh);
         }
    }
}
