﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_HOCKI
    {
        string _hocki;

        public string Hocki
        {
            get { return _hocki; }
            set { _hocki = value; }
        }
        string _mamon;

        public string Mamon
        {
            get { return _mamon; }
            set { _mamon = value; }
        }
        public DTO_HOCKI(string thocki)
        {
            _hocki = thocki;
        }
        public DTO_HOCKI(string thocki,string tmamon)
        {
            _hocki = thocki;
            _mamon = tmamon;
        }
        
    }
}
