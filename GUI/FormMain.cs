﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;
using DAL;
using System.Data.SqlClient;
using System.IO;
namespace GUI
{
    public partial class txtimahepath : Form
    {
        public static string phanquyen;
        public static DataTable dt = new DataTable();
        public txtimahepath()
        {

            InitializeComponent();
       
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dtgSINHVIEN.CurrentRow.Index;
            txtmasinhvien.Text = dtgSINHVIEN[0, index].Value.ToString();
            txthoten.Text = dtgSINHVIEN[1, index].Value.ToString();
            dateTimePicker_ngaysinh.Text = dtgSINHVIEN[2, index].Value.ToString();
            cmbgioitinh.Text = dtgSINHVIEN[3, index].Value.ToString();
            txtdiachi.Text = dtgSINHVIEN[4, index].Value.ToString();
            txtmalop.Text = dtgSINHVIEN[5, index].Value.ToString();

            ImageConverter objImageConverter = new ImageConverter();
            pictureBox1.Image = (Image)objImageConverter.ConvertFrom(dtgSINHVIEN[6, index].Value);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            //txtimagepath.Text = dtgSINHVIEN[7, index].Value.ToString();
            

        }
        void binding()
        {
            txtmasinhvien.DataBindings.Add(new Binding("Text",dtgSINHVIEN.DataSource,"MASV"));
            txthoten.DataBindings.Add(new Binding("Text", dtgSINHVIEN.DataSource, "HOTEN"));
            dateTimePicker_ngaysinh.DataBindings.Add(new Binding("Text", dtgSINHVIEN.DataSource, "NGAYSINH"));
            cmbgioitinh.DataBindings.Add(new Binding("Text", dtgSINHVIEN.DataSource, "GIOITINH"));
            txtdiachi.DataBindings.Add(new Binding("Text", dtgSINHVIEN.DataSource, "DIACHI"));
            txtmalop.DataBindings.Add(new Binding("Text", dtgSINHVIEN.DataSource, "MALOP"));
        }
        private void FormMain_Load(object sender, EventArgs e)
        {
            dt = BLL_SINHVIEN.laydulieu();
            dtgSINHVIEN.DataSource = dt;
            if (phanquyen == "0")
            {
                btnthem.Enabled = false;
                btnsua.Enabled = false;
                btnxoa.Enabled = false;
                cậpNhậtĐiểmToolStripMenuItem.Enabled = false;
                phanquyentoolstripmenuitem.Enabled = false;
                quaToolStripMenuItem.Enabled = false;
 
            }
            if(phanquyen == "0")
            labelxinchao.Text = " Xin chào : " + Formlogin.b + " -- Quyền : USER";
            if (phanquyen == "1")
                labelxinchao.Text = " Xin chào : " + Formlogin.b + " -- Quyền : ADMIN";
            //DateTimePicker dtp = new DateTimePicker();
            //dtp.Format = DateTimePickerFormat.Custom;
            //dtp.CustomFormat = "dd/MM/yyyy";
            //this.Controls.Add(dtp);
           // binding();
        }
        
        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void đĂNGXUẤTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Formlogin.dem == 1)
            {
                Formlogin frmlogin = new Formlogin(Formlogin.a, Formlogin.b);
                this.Close();
                frmlogin.Show();
            }
            else
            {
                Formlogin frm = new Formlogin();
                this.Close();
                frm.Show();
            }
        }
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }
        public bool quyen { get; set; }
        private void btnthem_Click(object sender, EventArgs e)
        {
            try
            {
                int n = Int32.Parse(txtmasinhvien.Text);
                if (n <= 1)
                {
                    MessageBox.Show("Loi ! Hay nhap lai gia tri!");
                }
                else
                {
                    DTO_SINHVIEN dtsv = new DTO_SINHVIEN(txtmasinhvien.Text, txthoten.Text, dateTimePicker_ngaysinh.Value, cmbgioitinh.Text, txtdiachi.Text, txtmalop.Text.ToUpper(), convertinmagetobyte());
                    BLL_SINHVIEN.them(dtsv);
                    dtgSINHVIEN.DataSource = BLL_SINHVIEN.laydulieu();
                    MessageBox.Show("Thêm Thành Công");
                }
                

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                DAL_SINHVIEN.con.Close();
            }
        }
        private void btnxoa_Click(object sender, EventArgs e)
        {
            try
            {
                DTO_SINHVIEN dtsv = new DTO_SINHVIEN(txtmasinhvien.Text);
                BLL_SINHVIEN.xoa(dtsv);
                dtgSINHVIEN.DataSource = BLL_SINHVIEN.laydulieu();
                button3_Click(sender, e);
                MessageBox.Show("Xoá Thành Công");
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                DAL_SINHVIEN.con.Close();
            }
                
        }
        private void btnsua_Click(object sender, EventArgs e)
        {
          try
          {
              DTO_SINHVIEN dtsv = new DTO_SINHVIEN(txtmasinhvien.Text, txthoten.Text, dateTimePicker_ngaysinh.Value, cmbgioitinh.Text, txtdiachi.Text, txtmalop.Text.ToUpper(), convertinmagetobyte());
              BLL_SINHVIEN.sua(dtsv);
              dtgSINHVIEN.DataSource = BLL_SINHVIEN.laydulieu();
              MessageBox.Show("Sửa Thành Công");
          }
          catch (System.Exception ex)
          {
              MessageBox.Show(ex.Message);
          }
          finally
          {
              DAL_SINHVIEN.con.Close();
          }
              
        }
        private void dateTimePicker_ngaysinh_ValueChanged(object sender, EventArgs e)
        {
            dateTimePicker_ngaysinh.Value.ToShortDateString();
        }

        private void thêmTàiKhoảnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_doimatkhau frmstt = new Form_doimatkhau();
            frmstt.ShowDialog();
        }

        private void cậpNhậtĐiểmToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            cmbtenmon frm = new cmbtenmon();
            frm.ShowDialog();
        }

        private void cmbkhoa_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmblop_SelectedIndexChanged(object sender, EventArgs e)
        {
            // string malop = cmblop.SelectedValue.ToString();

        }

        private void grbtimkiem_Enter(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {


        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }


        public byte[] convertinmagetobyte()
        {
            FileStream fs;
            fs = new FileStream(txtimagepath.Text, FileMode.Open, FileAccess.Read);
            byte[] picbyte = new byte[fs.Length];
            fs.Read(picbyte, 0, System.Convert.ToInt32(fs.Length));
            fs.Close();
            return picbyte;
        }



        private void xEMĐIỂMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnxemdiem frxd = new btnxemdiem();
            frxd.ShowDialog();
        }

        private void toolTip1_Popup_1(object sender, PopupEventArgs e)
        {

        }  
        private void button3_Click(object sender, EventArgs e)
        {
            txthoten.ResetText();
            txtdiachi.ResetText();
            txtmalop.ResetText();
            txtmasinhvien.ResetText();
            cmbgioitinh.ResetText();
            dateTimePicker_ngaysinh.ResetText();
            txtmasinhvien.Focus();
            pictureBox1.ResumeLayout();
            checkBox1.Checked = false;
            dtgSINHVIEN.DataSource = BLL_SINHVIEN.laydulieu();
        }

        private void quarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = openFileDialog1.Filter = "JPG files (*.jpg)|*.jpg|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    pictureBox1.ImageLocation = openFileDialog1.FileName;
                    txtimagepath.Text = openFileDialog1.FileName;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void txtimagepath_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void phânQuyềnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_phanquyen frmpq = new Form_phanquyen();
            frmpq.ShowDialog();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked == true)
            {
                pictureBox1.Image = GUI.Properties.Resources.avatar;
                txtimagepath.Text = @"..\avatar.jpg";
                
            }
            
        }

        private void khoaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FORM_KHOA frmkhoa = new FORM_KHOA();
            frmkhoa.ShowDialog();
        }

        private void mônHọcToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_MONHOC frmmonhoc = new Form_MONHOC();
            frmmonhoc.ShowDialog();
        }

        private void họcKìToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formhocki frmhk = new Formhocki();
            frmhk.ShowDialog();
        }

        private void lớpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FORM_LOP frmlop = new FORM_LOP();
            frmlop.ShowDialog();
        }

        private void thToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void thôngTinSinhViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formrepprtthongtinsv rp = new Formrepprtthongtinsv();
            rp.ShowDialog();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            DTO_SINHVIEN dtsv = new  DTO_SINHVIEN(txtmasinhvien.Text);
            dtgSINHVIEN.DataSource = BLL_SINHVIEN.timkiemsv(dtsv);
            
        }

        private void điểmSinhViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formreportdiemsv rpdiem = new Formreportdiemsv();
            rpdiem.ShowDialog();
        }

        private void txtmasinhvien_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
           
        }

        private void button2_Click_2(object sender, EventArgs e)
        {
            DTO_LOP dtsv = new DTO_LOP(txtmalop.Text);
            dtgSINHVIEN.DataSource = BLL_SINHVIEN.timkiemmalop(dtsv);
            
        }

        private void thốngKêToolStripMenuItem_Click(object sender, EventArgs e)
        {
            form_thongke frmthongke = new form_thongke();
            frmthongke.ShowDialog();
        }

        private void tìmKiếmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm_timkiem frmtimkiem = new Frm_timkiem();
            frmtimkiem.ShowDialog();
        }

        private void liênHệToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_lienhe frmlienhe = new Form_lienhe();
            frmlienhe.ShowDialog();
        }
    }
}
