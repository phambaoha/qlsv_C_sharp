﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace DAL
{
    public class DAL_HOCKI
    {
        static string constring = ConfigurationManager.ConnectionStrings["qldiemsv"].ToString();
        static SqlConnection con = new SqlConnection(constring);
        public static DataTable loadhocki()
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from table_hocki", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable loaddlcmbmamon(DTO_HOCKI hk)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select distinct mh.mamon from Table_hocki hk inner join Table_MONHOC mh on mh.hocki = hk.hocki where hk.hocki = @hocki", con);
            cmd.Parameters.AddWithValue("@hocki", hk.Hocki);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable loaddlcmbhocki()
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select distinct hocki from table_hocki", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static void themhocki (DTO_HOCKI hk)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into table_hocki values(@hocki,@mamon)", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@hocki",hk.Hocki);
            cmd.Parameters.AddWithValue("@mamon",hk.Mamon);
            cmd.ExecuteNonQuery();         
            con.Close();
        }
        public static void suahocki(DTO_HOCKI hk)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("update table_hocki set hocki = @hocki,mamon = @mamon where hocki = @hocki and mamon = @mamon", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@hocki", hk.Hocki);
            cmd.Parameters.AddWithValue("@mamon", hk.Mamon);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void xoahocki(DTO_HOCKI hk)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("delete from table_hocki  where hocki = @hocki and mamon = @mamon ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@hocki", hk.Hocki);
            cmd.Parameters.AddWithValue("@mamon", hk.Mamon);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static DataTable hocbongsinhvien(DTO_HOCKI hk)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(" select kq.hocki as N'Học Kì',kq.Masv as N'Mã SV',kq.hoten as N'Họ Tên', cast((sum(kq.diemtonghebon)/sum(sotc)) as real) as N'Điểm Tích Luỹ', kq.diemrenluyen,kq.malop as 'Mã Lớp 'from (Table_SINHVIEN sv inner join Table_KetQua kq  on sv.masv = kq.MASV) inner join Table_hocki hk on kq.hocki = hk.hocki  where hk.hocki = @hocki and kq.diemrenluyen >=80   group by kq.hocki,kq.Masv,kq.hoten,kq.diemrenluyen,kq.malop having cast((sum(kq.diemtonghebon)/sum(sotc)) as real)>=3.2", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@hocki", hk.Hocki);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable canhcaosinhvien(DTO_HOCKI hk)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(" select kq.hocki as N'Học Kì',kq.Masv as N'Mã SV',kq.hoten as N'Họ Tên', cast((sum(kq.diemtonghebon)/sum(sotc)) as real) as N'Điểm Tích Luỹ', kq.diemrenluyen,kq.malop as N'Mã lớp' from (Table_SINHVIEN sv inner join Table_KetQua kq  on sv.masv = kq.MASV) inner join Table_hocki hk on kq.hocki = hk.hocki  where hk.hocki = @hocki and kq.diemrenluyen <50  group by kq.hocki,kq.Masv,kq.hoten,kq.diemrenluyen,kq.malop having cast((sum(kq.diemtonghebon)/sum(sotc)) as real)<2.0 ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@hocki", hk.Hocki);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        } 
    }
}
