﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;

namespace BLL
{
    public class BLL_LOGIN
    {
        public static DataTable laythongtindangnhap(DTO_LOGIN log)
        {
            return DAL_LOGIN.laythongtindangnhap(log);
        }
        public static DataTable thongtinphanquyen(DTO_LOGIN log)
        {
            return DAL_LOGIN.thongtinphanquyen(log);
        }
        public static DataTable doimatkhau(DTO_LOGIN log)
        {
            return DAL_LOGIN.doimatkhau(log);
             
        }
          public static void suamatkhau (DTO_LOGIN log)
        {
            DAL_LOGIN.suamatkhau(log);
        }
         public static DataTable loadthongtinformphanquyen()
          {
             return DAL_LOGIN.loadthongtinformphanquyen();
          }
        public static void themphanquyen(DTO_LOGIN log)
         {
              DAL_LOGIN.themphanquyen(log);
         }
         public static void suaphanquyen(DTO_LOGIN log)
        {
            DAL_LOGIN.suaphanquyen(log);
        }
        public static void xoaphanquyen(DTO_LOGIN log)
         {
             DAL_LOGIN.xoaphanquyen(log);
         }

    }
}
