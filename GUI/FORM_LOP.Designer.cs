﻿namespace GUI
{
    partial class FORM_LOP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FORM_LOP));
            this.label1 = new System.Windows.Forms.Label();
            this.btnthemlop = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnsualop = new System.Windows.Forms.Button();
            this.btnxoalop = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtmalop = new System.Windows.Forms.TextBox();
            this.txtmakhoa = new System.Windows.Forms.TextBox();
            this.txttenlop = new System.Windows.Forms.TextBox();
            this.dtglop = new System.Windows.Forms.DataGridView();
            this.malop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenlop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.makhoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtglop)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(404, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã Lớp";
            // 
            // btnthemlop
            // 
            this.btnthemlop.ForeColor = System.Drawing.Color.Blue;
            this.btnthemlop.Location = new System.Drawing.Point(407, 197);
            this.btnthemlop.Name = "btnthemlop";
            this.btnthemlop.Size = new System.Drawing.Size(75, 23);
            this.btnthemlop.TabIndex = 1;
            this.btnthemlop.Text = "Thêm";
            this.btnthemlop.UseVisualStyleBackColor = true;
            this.btnthemlop.Click += new System.EventHandler(this.btnthemlop_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(404, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên Lớp";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(404, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Mã Khoa";
            // 
            // btnsualop
            // 
            this.btnsualop.ForeColor = System.Drawing.Color.Blue;
            this.btnsualop.Location = new System.Drawing.Point(502, 197);
            this.btnsualop.Name = "btnsualop";
            this.btnsualop.Size = new System.Drawing.Size(75, 23);
            this.btnsualop.TabIndex = 4;
            this.btnsualop.Text = "Sửa";
            this.btnsualop.UseVisualStyleBackColor = true;
            this.btnsualop.Click += new System.EventHandler(this.btnsualop_Click);
            // 
            // btnxoalop
            // 
            this.btnxoalop.ForeColor = System.Drawing.Color.Blue;
            this.btnxoalop.Location = new System.Drawing.Point(407, 245);
            this.btnxoalop.Name = "btnxoalop";
            this.btnxoalop.Size = new System.Drawing.Size(75, 23);
            this.btnxoalop.TabIndex = 5;
            this.btnxoalop.Text = "Xoá";
            this.btnxoalop.UseVisualStyleBackColor = true;
            this.btnxoalop.Click += new System.EventHandler(this.btnxoalop_Click);
            // 
            // button4
            // 
            this.button4.ForeColor = System.Drawing.Color.Blue;
            this.button4.Location = new System.Drawing.Point(502, 245);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 6;
            this.button4.Text = "Thoát";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtmalop
            // 
            this.txtmalop.Location = new System.Drawing.Point(460, 42);
            this.txtmalop.Name = "txtmalop";
            this.txtmalop.Size = new System.Drawing.Size(139, 20);
            this.txtmalop.TabIndex = 7;
            // 
            // txtmakhoa
            // 
            this.txtmakhoa.Location = new System.Drawing.Point(460, 115);
            this.txtmakhoa.Name = "txtmakhoa";
            this.txtmakhoa.Size = new System.Drawing.Size(139, 20);
            this.txtmakhoa.TabIndex = 8;
            // 
            // txttenlop
            // 
            this.txttenlop.Location = new System.Drawing.Point(460, 81);
            this.txttenlop.Name = "txttenlop";
            this.txttenlop.Size = new System.Drawing.Size(139, 20);
            this.txttenlop.TabIndex = 9;
            // 
            // dtglop
            // 
            this.dtglop.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtglop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtglop.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.malop,
            this.tenlop,
            this.makhoa});
            this.dtglop.Location = new System.Drawing.Point(12, 33);
            this.dtglop.Name = "dtglop";
            this.dtglop.Size = new System.Drawing.Size(371, 235);
            this.dtglop.TabIndex = 10;
            this.dtglop.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtglop_CellContentClick);
            // 
            // malop
            // 
            this.malop.DataPropertyName = "malop";
            this.malop.HeaderText = "Mã lớp";
            this.malop.Name = "malop";
            // 
            // tenlop
            // 
            this.tenlop.DataPropertyName = "tenlop";
            this.tenlop.HeaderText = "Tên lớp";
            this.tenlop.Name = "tenlop";
            // 
            // makhoa
            // 
            this.makhoa.DataPropertyName = "makhoa";
            this.makhoa.HeaderText = "Mã Khoa";
            this.makhoa.Name = "makhoa";
            // 
            // FORM_LOP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gold;
            this.ClientSize = new System.Drawing.Size(617, 290);
            this.Controls.Add(this.dtglop);
            this.Controls.Add(this.txttenlop);
            this.Controls.Add(this.txtmakhoa);
            this.Controls.Add(this.txtmalop);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnxoalop);
            this.Controls.Add(this.btnsualop);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnthemlop);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FORM_LOP";
            this.Text = "Lớp";
            this.Load += new System.EventHandler(this.FORM_LOP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtglop)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnthemlop;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnsualop;
        private System.Windows.Forms.Button btnxoalop;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtmalop;
        private System.Windows.Forms.TextBox txtmakhoa;
        private System.Windows.Forms.TextBox txttenlop;
        private System.Windows.Forms.DataGridView dtglop;
        private System.Windows.Forms.DataGridViewTextBoxColumn malop;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenlop;
        private System.Windows.Forms.DataGridViewTextBoxColumn makhoa;
    }
}