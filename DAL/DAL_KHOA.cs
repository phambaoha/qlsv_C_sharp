﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace DAL
{
    public class DAL_KHOA
    {
        static string constring = ConfigurationManager.ConnectionStrings["qldiemsv"].ToString();
        static SqlConnection con = new SqlConnection(constring);
        public static DataTable laydlmakhoa()
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from table_khoa", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
        public static DataTable loadcomboboxmalop(DTO_KHOA kh)
        {  
            SqlCommand cmd = new SqlCommand("select distinct lp.MaLop from Table_Lop lp inner join Table_khoa kh on lp.MAKHOA = kh.MaKHOA where kh.MaKHOA = @makhoa", con);
            cmd.Parameters.AddWithValue("@makhoa", kh.Makhoa);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close(); 
            return dt; 
        }
        public static void themkhoa(DTO_KHOA kh)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into table_khoa values(@makhoa,@tenkhoa)", con);
            cmd.Parameters.AddWithValue("@makhoa", kh.Makhoa);
            cmd.Parameters.AddWithValue("@tenkhoa", kh.Tenkhoa);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();  
            con.Close();
            
        }
        public static void suakhoa(DTO_KHOA kh)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("update table_khoa set tenkhoa = @tenkhoa where makhoa = @makhoa", con);
            cmd.Parameters.AddWithValue("@makhoa", kh.Makhoa);
            cmd.Parameters.AddWithValue("@tenkhoa", kh.Tenkhoa);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void xoakhoa(DTO_KHOA kh)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("delete from table_khoa where makhoa = @makhoa", con);
            cmd.Parameters.AddWithValue("@makhoa", kh.Makhoa);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
            con.Close();
        }

    }
}
